﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace metroid{
	
	public class BaseState : IState{
		
		public List<Entity> entities = new List<Entity>();
		public string name;
		public Game1 game;
		//map map.tmx

		public BaseState (Game1 g, string n){
			game = g;
			name = n;
		}

		public virtual void Enter(string lastState=null){}

		public virtual void HandleInput(){}

		public virtual void HandleInput(GameTime gameTime){}

		public virtual void Update(GameTime gameTime){}

		public virtual void Draw(GameTime gameTime){
			/*foreach(Entity e in entities) {
				e.Draw(gameTime);
			}*/
		}

		public virtual void Exit(string newState=null){}
	}
}

