﻿using System;
using Microsoft.Xna.Framework;

namespace metroid
{
	public interface IState
	{

		void Enter(string lastState=null);

		void HandleInput();

		void Update(GameTime gameTime);

		void Draw(GameTime gameTime);

		void Exit(string newState=null);
	}
}

