﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace metroid
{
	public class StateMachine
	{

		public Dictionary<string,BaseState> states;
		public BaseState activeState;

		public StateMachine (Dictionary<string,BaseState> s=null, string initialState=null)
		{
			if(s != null) {
				states = s;
				Start(initialState);
			}
		}

		public void Start(string startState){
			if(startState != null) {
				activeState = states[startState];
				activeState.Enter();
			}
		}


		public void Change(string newState){
            string lastState = activeState.name;
			activeState.Exit(newState);
			activeState = states[newState];
			activeState.Enter(lastState);
		}

		public void add(string state, BaseState state_){
			states[state] = state_;
			if(activeState == null) {
				activeState = state_;
				activeState.Enter();
			}
		}

		public void Set(string state, BaseState state_){
			states[state] = state_;
		}

		public BaseState get(string state){
			return states[state];
		}

		public void Update(GameTime gameTime){
			if(activeState != null) {
				activeState.Update(gameTime);
			}
		}

		public void Draw(GameTime gameTime){
			if(activeState != null) {
				activeState.Draw(gameTime);
			}
		}


	}
}

