﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace metroid{
	
	public class Game1 : Game {

		
		public World world;
		
		public GraphicsDeviceManager graphics;
		public SpriteBatch spriteBatch;
		public Texture2D tileset;
		public Texture2D samusSprites;
        public Texture2D samusDeathSprites;
		public Texture2D enemySprites;
		public Room r;

        public static Keys UP = Keys.Up;
        public static Keys DOWN = Keys.Down;
        public static Keys LEFT = Keys.Left;
        public static Keys RIGHT = Keys.Right;
        public static Keys JUMP = Keys.X;
        public static Keys SHOOT = Keys.Z;
        public static Keys START = Keys.Enter;
        public static Keys SELECT = Keys.LeftShift;


		public bool debug = false;
		public bool borderDebug = false;
		public bool cameraDebug = false;

        public SoundEffectInstance music;
        public SoundEffectInstance musicGetItem;
        public SoundEffectInstance sfx;
        public SoundEffect musicTitle;
        public SoundEffect musicSpawn;
        public SoundEffect musicBrinstar;
        public SoundEffect musicItem;
        public SoundEffect musicItemRoom;
        public SoundEffect sfxShoot;
        public SoundEffect sfxMissile;
        public SoundEffect sfxDamageSamus;
        public SoundEffect sfxDamageEnemy;
        public SoundEffect sfxDamageRipper;
        public SoundEffect sfxDoor;
        public SoundEffect sfxJump;
        public SoundEffect sfxDeath;
        public SoundEffect sfxGetEnergy;

        public SpriteFont nes;
        public SpriteFont nesHud;
        public Texture2D[] titleScreen;
        public string gameState = "title";
        public bool title = true;
        public int nextTitle = 0;
        float timer = 24f;
        public bool startPressed = false;
        public bool selectPressed = false;
        public bool menuStartSelected = true;
        public Vector2 menuCursor;
        public int startRoom = 0;
        public bool invulnerable = false;
        public bool f5pressed = false;
        public bool gamePadConnected = false;


		public Game1()
		{
			graphics = new GraphicsDeviceManager(this);
			graphics.PreferredBackBufferWidth = 512;
			graphics.PreferredBackBufferHeight = 448;
			Content.RootDirectory = "Content";
            menuCursor = new Vector2(170,200);

		}


		protected override void Initialize()
		{

            
			base.Initialize();
		}


		protected override void LoadContent(){
	
			spriteBatch = new SpriteBatch(GraphicsDevice);
			tileset = Content.Load<Texture2D>("maps/master_tileset");
			samusSprites = Content.Load<Texture2D>("sprites/samus");
            samusDeathSprites = Content.Load<Texture2D>("sprites/samus_death");
			enemySprites = Content.Load<Texture2D>("sprites/enemy");
			
            nes = Content.Load<SpriteFont>("fonts/nes");
            nesHud = Content.Load<SpriteFont>("fonts/nes_hud");

            musicTitle = Content.Load<SoundEffect>("sound/music_title");
            musicSpawn = Content.Load<SoundEffect>("sound/music_spawn");
            musicBrinstar = Content.Load<SoundEffect>("sound/music_brinstar");
            musicItem = Content.Load<SoundEffect>("sound/music_item_get");
            musicItemRoom = Content.Load<SoundEffect>("sound/music_item_room");
            sfxShoot = Content.Load<SoundEffect>("sound/sfx_shoot");
            sfxMissile = Content.Load<SoundEffect>("sound/sfx_missile");
            sfxDamageSamus = Content.Load<SoundEffect>("sound/sfx_damage_samus");
            sfxDamageEnemy = Content.Load<SoundEffect>("sound/sfx_damage_enemy");
            sfxDamageRipper = Content.Load<SoundEffect>("sound/sfx_damage_ripper");
            sfxDoor = Content.Load<SoundEffect>("sound/sfx_door");
            sfxJump = Content.Load<SoundEffect>("sound/sfx_jump_samus");
            sfxDeath = Content.Load<SoundEffect>("sound/sfx_samus_death");
            sfxGetEnergy = Content.Load<SoundEffect>("sound/sfx_life_get");
            music = musicTitle.CreateInstance();
            music.IsLooped = true;
            musicGetItem = musicItem.CreateInstance();
            musicGetItem.IsLooped = false;
            //sfx = sfxShoot.CreateInstance();
            //sfx.IsLooped = false;
            titleScreen = new Texture2D[96];
            int c = 0;
            for(int i = 5; i < 101; i++){
                titleScreen[c] = Content.Load<Texture2D>("title/"+i);
                c++;
            }

            if(!title){
                world = new World(this);
            }
            music.Play();
		}

        public void DrawText(string t, Vector2 p){
            spriteBatch.DrawString (
                nes, 
                t, 
                p, 
                Color.White, 
                0.0f, 
                new Vector2 (0, 0), 
                Vector2.One, 
                SpriteEffects.None,
                0.0f
            );
        }

        public void DrawText(string t, Vector2 p, Color c){
            spriteBatch.DrawString (
                nes, 
                t, 
                p, 
                c, 
                0.0f, 
                new Vector2 (0, 0), 
                Vector2.One, 
                SpriteEffects.None,
                0.0f
            );
        }

        public void DrawText(string t, Vector2 p, SpriteFont f){
            spriteBatch.DrawString (
                f, 
                t, 
                p, 
                Color.White, 
                0.0f, 
                new Vector2 (0, 0), 
                Vector2.One, 
                SpriteEffects.None,
                0.0f
            );
        }

        public bool Pressed(string k){
            if(GamePad.GetState(0).IsConnected){
                GamePadState s = GamePad.GetState(0);
                if(k == "START" && s.IsButtonDown(Buttons.Back)){
                    return true;
                }
                if(k == "SELECT" && s.IsButtonDown(Buttons.Start)){
                    return true;
                }
                if(k == "JUMP" && s.IsButtonDown(Buttons.B)){
                    return true;
                }
                if(k == "SHOOT" && s.IsButtonDown(Buttons.BigButton)){
                    return true;
                }
                if(k == "UP" && s.IsButtonDown(Buttons.DPadUp)){
                    return true;
                }
                if(k == "DOWN" && s.IsButtonDown(Buttons.DPadDown)){
                    return true;
                }
                if(k == "LEFT" && s.IsButtonDown(Buttons.DPadLeft)){
                    return true;
                }
                if(k == "RIGHT" && s.IsButtonDown(Buttons.DPadRight)){
                    return true;
                }
            } else {
                KeyboardState s = Keyboard.GetState();
                if(k == "START" && s.IsKeyDown(Keys.Enter)){
                    return true;
                }
                if(k == "SELECT" && s.IsKeyDown(Keys.LeftShift)){
                    return true;
                }
                if(k == "JUMP" && s.IsKeyDown(Keys.X)){
                    return true;
                }
                if(k == "SHOOT" && s.IsKeyDown(Keys.Z)){
                    return true;
                }
                if(k == "UP" && s.IsKeyDown(Keys.Up)){
                    return true;
                }
                if(k == "DOWN" && s.IsKeyDown(Keys.Down)){
                    return true;
                }
                if(k == "LEFT" && s.IsKeyDown(Keys.Left)){
                    return true;
                }
                if(k == "RIGHT" && s.IsKeyDown(Keys.Right)){
                    return true;
                }
            }
            return false;
        }

        public bool Released(string k){
            if(GamePad.GetState(0).IsConnected){
                GamePadState s = GamePad.GetState(0);
                if(k == "START" && s.IsButtonUp(Buttons.Back)){
                    return true;
                }
                if(k == "SELECT" && s.IsButtonUp(Buttons.Start)){
                    return true;
                }
                if(k == "JUMP" && s.IsButtonUp(Buttons.B)){
                    return true;
                }
                if(k == "SHOOT" && s.IsButtonUp(Buttons.BigButton)){
                    return true;
                }
                if(k == "UP" && s.IsButtonUp(Buttons.DPadUp)){
                    return true;
                }
                if(k == "DOWN" && s.IsButtonUp(Buttons.DPadDown)){
                    return true;
                }
                if(k == "LEFT" && s.IsButtonUp(Buttons.DPadLeft)){
                    return true;
                }
                if(k == "RIGHT" && s.IsButtonUp(Buttons.DPadRight)){
                    return true;
                }
            } else {
                KeyboardState s = Keyboard.GetState();
                if(k == "START" && s.IsKeyUp(Keys.Enter)){
                    return true;
                }
                if(k == "SELECT" && s.IsKeyUp(Keys.LeftShift)){
                    return true;
                }
                if(k == "JUMP" && s.IsKeyUp(Keys.X)){
                    return true;
                }
                if(k == "SHOOT" && s.IsKeyUp(Keys.Z)){
                    return true;
                }
                if(k == "UP" && s.IsKeyUp(Keys.Up)){
                    return true;
                }
                if(k == "DOWN" && s.IsKeyUp(Keys.Down)){
                    return true;
                }
                if(k == "LEFT" && s.IsKeyUp(Keys.Left)){
                    return true;
                }
                if(k == "RIGHT" && s.IsKeyUp(Keys.Right)){
                    return true;
                }
            }
            return false;
        }


        public bool Start(){
            if(Pressed("START") && !startPressed){
                startPressed = true;
            }
            if(Released("START") && startPressed){
                startPressed = false;
                return true;
            }
            return false;
        }

        public bool Select(){
            if(Pressed("SELECT") && !selectPressed){
                selectPressed = true;
            }
            if(Released("SELECT") && selectPressed){
                selectPressed = false;
                return true;
            }
            return false;
        }

        public bool F5(){
            if(Keyboard.GetState().IsKeyDown(Keys.F5) && !f5pressed){
                f5pressed = true;
            }
            if(Keyboard.GetState().IsKeyUp(Keys.F5) && f5pressed){
                f5pressed = false;
                return true;
            }
            return false;
        }

	
		protected override void Update(GameTime gameTime){


            base.Update(gameTime);
			#if !__IOS__ &&  !__TVOS__
			if(Keyboard.GetState().IsKeyDown(Keys.Escape))
				Exit();
			#endif

            if(gameState == "play"){
                if(F5()){
                    invulnerable = !invulnerable;
                    sfxMissile.Play();
                }

                if(Keyboard.GetState().IsKeyDown(Keys.F1)) {
                    debug = !debug;
			    }

                if(Keyboard.GetState().IsKeyDown(Keys.F2)) {
                    borderDebug = !borderDebug;
			    }

                if(Keyboard.GetState().IsKeyDown(Keys.F3)) {
                    cameraDebug = !cameraDebug;
			    }

                if(Start()){
                    world.Pause();
                }
                    
                world.Update(gameTime);


            } else if(gameState == "menu"){
                if(Start()){
                    if(menuStartSelected){
                        world = new World(this);
                        gameState = "play";
                    } else {
                        gameState = "continue";
                        menuCursor.X = 30;
                        menuCursor.Y = 50;
                    }
                }
                if(Select()){
                    if(menuStartSelected){
                        menuStartSelected = false;
                        menuCursor.Y = 250;
                    } else {
                        menuStartSelected = true;
                        menuCursor.Y = 200;
                    }
                }
                    
            } else if(gameState == "continue"){
                if(Select()){
                    startRoom++;
                    menuCursor.Y += 30;
                    if(startRoom > 12){
                        startRoom = 0;
                        menuCursor.Y = 50;
                    }
                }
                if(Start()){
                    world = new World(this, "room_" + startRoom);
                    gameState = "play";
                }
            } else if(gameState == "over"){
                music.Stop();
                if(Start()){
                    gameState = "continue";
                    menuCursor.X = 30;
                    menuCursor.Y = 50;
                    startRoom = 0;
                }
            } else if(gameState == "title"){
                

                //GamePad.


                if(Start()){
                    gameState = "menu";
                }
            }
            
			
		}


		protected override void Draw(GameTime gameTime){
			graphics.GraphicsDevice.Clear(Color.Black);
			spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.NonPremultiplied, SamplerState.PointClamp);
            if(gameState == "title"){
                spriteBatch.Draw(
                    titleScreen[nextTitle],
                    new Vector2(0, 0),
                    null,
                    null,
                    Vector2.Zero,
                    0.0f
                );
                if(timer <= 0f){
                    timer = 24f;
                    nextTitle++;
                    if(nextTitle == titleScreen.Length){
                        nextTitle = 0;
                    }
                } else {
                    timer -= (float)gameTime.ElapsedGameTime.Milliseconds / 10f;
                }

            } else if(gameState == "menu") {
                DrawText("START", new Vector2(200, 190), Color.Aquamarine);
                DrawText("CONTINUE", new Vector2(200, 240), Color.Aquamarine);
                spriteBatch.Draw(
                    samusSprites,
                    menuCursor,
                    new Rectangle(90, 1443, 16, 16),
                    Color.White,
                    0.0f,
                    new Vector2(0, 0),
                    1.0f,
                    SpriteEffects.None,
                    0.75f
                );
            } else if(gameState == "continue") { 
                DrawText("CHOOSE STARTING ROOM:", new Vector2(30, 10), Color.Aquamarine);
                for(int i = 0; i < 13; i++){
                    DrawText("ROOM " + i, new Vector2(60, 40 + (30 * i)), Color.Aquamarine);
                }
                spriteBatch.Draw(
                    samusSprites,
                    menuCursor,
                    new Rectangle(90, 1443, 16, 16),
                    Color.White,
                    0.0f,
                    new Vector2(0, 0),
                    1.0f,
                    SpriteEffects.None,
                    0.75f
                );
            } else if(gameState == "play"){
                world.Draw(gameTime);
            } else if(gameState == "over"){
                DrawText("GAME OVER", new Vector2(170, 190), Color.Aquamarine);
            }
			//gameState.Draw(gameTime);

			spriteBatch.End();
			base.Draw(gameTime);
		}
	}
}

