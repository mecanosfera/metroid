﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace metroid{
	
	public class Item : Entity{

        public string type;
        public bool taken = false;
        public float despawnTimer=0f;
        public Enemy dropper;
        public float animationTimer = 0f;
        public int animationIndex = 0;

        public Item(Room r, string n,string t, Vector2 pos) : base(r,n){
            type = t;
            blockSize = 1;
            width = 1;
            height = 1;
            if (type == "item"){
                hitBox = new Rectangle(0, 0, 32, 32);
                if (name == "marumari"){
                    animation = new Animation[] {new Animation(new Vector2(2,2),new Rectangle(67,1338,27,29),200.0f) };  
                } else if (name == "energy_tank"){
                    animation = new Animation[] {new Animation(new Vector2(-2,-4),new Rectangle(116,1334,36,36),200.0f) };
                } else if (name == "missile"){
                    animation = new Animation[] {new Animation(new Vector2(-2,-4),new Rectangle(16,1333,36,36),200.0f) };
                }
            } else {
                despawnTimer = 16f;
                if (name == "energy"){
                    hitBox = new Rectangle(8, 8, 16, 16);
                    animation = new Animation[] {
                        new Animation(new Vector2(8,8),new Rectangle(38,1411,16,16),6.0f), 
                        new Animation(new Vector2(8,8),new Rectangle(70,1411,16,16),6.0f), 
                    };
                } else if (name == "missile"){
                    hitBox = new Rectangle(8, 8, 16, 24);
                    animation = new Animation[] {
                        new Animation(new Vector2(8,4),new Rectangle(184,1410,16,24),200.0f),
                        new Animation(new Vector2(8,4),new Rectangle(184,1410,16,24),200.0f)
                    };
                }
                //room.dropItens.Add(this);
            }
            position = pos;
        }

        public override void Update(GameTime gameTime){
            //Console.WriteLine("aaaaaaaa");
            if(!taken && TestCollision(room.player)){
                if(type == "item"){
                    room.player.states.Change("fall");
                    room.getItem = true;
                    game.music.Pause();
                    game.musicGetItem.Play();
                    if(name == "marumari"){
                        room.player.maruMari = true;
                    } else if(name == "energy_tank"){
                        room.player.maxEnergyTanks += 1;
                        room.player.fullEnergyTanks = 1;
                        room.player.hp = 99;
                    } else if(name == "missile"){
                        room.player.maxMissiles += 5;
                        room.player.missiles += 5;
                    }
                } else {
                    game.sfxGetEnergy.Play();
                    if(name == "energy"){
                        room.player.SetHp(5);
                    } else if(name == "missile"){
                        if(room.player.missiles < room.player.maxMissiles){
                            room.player.missiles++;
                        }
                    }
                    taken = true;
                }
            } else if(!taken && type=="drop" && despawnTimer > 0f){
                despawnTimer -= gameTime.ElapsedGameTime.Milliseconds / 10f;
                if(despawnTimer <= 0f){
                    despawnTimer = 0f;
                    taken = true;
                }
            }
        }

        public void Spawn(Vector2 p,string n="energy"){
            if(type=="drop"){
                position = p;
                name = n;
                taken = false;
                despawnTimer = 720f;
                if (name == "energy"){
                    hitBox = new Rectangle(8, 8, 16, 16);
                    animation = new Animation[] {
                        new Animation(new Vector2(8,8),new Rectangle(38,1411,16,16),6.0f), 
                        new Animation(new Vector2(8,8),new Rectangle(70,1411,16,16),6.0f) 
                    };
                } else if (name == "missile"){
                    hitBox = new Rectangle(8, 8, 16, 24);
                    animation = new Animation[] {
                        new Animation(new Vector2(8,4),new Rectangle(184,1410,16,24),200.0f),
                        new Animation(new Vector2(8,4),new Rectangle(184,1410,16,24),200.0f)//,
                        //new Animation(new Vector2(1,1),new Rectangle(1,1,1,1),200.0f) 
                    };
                }
            }
        }

        public Animation nextSprite(){
            Animation sprite;
            animationIndex++;
            if(animationIndex >= animation.Length) {
                animationIndex = 0;
            }
            sprite = animation[animationIndex];
            return sprite;
        }

        public override void Draw(GameTime gameTime){
            if(!taken){
                Animation sprite;
                float time = gameTime.ElapsedGameTime.Milliseconds/10.0f;
               
                animationTimer -= time;
                if(animationTimer <= 0.0f) {
                    sprite = nextSprite(); 
                    animationTimer = sprite.time;
                } else {
                    sprite = animation[animationIndex];
                }
               
                SpriteEffects se = SpriteEffects.None;
                float spritePosX = sprite.sheetPos.X;


                game.spriteBatch.Draw(
                    room.player.spriteSheet,
                    new Vector2(
                        position.X-room.cameraPos.X+spritePosX,
                        position.Y-room.cameraPos.Y+sprite.sheetPos.Y
                    ),
                    sprite.drawArea,
                    Color.White,
                    0.0f,
                    new Vector2(0,0),
                    1.0f,
                    se,
                    0.75f

                );    
            }

        }

	}
}

