﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace metroid {
	
	public class SamusBullet : Bullet {

		public Samus player;
        public bool missile = false;
        public Rectangle[] normalBullet;
        public Rectangle[] missileBullet;
        public Rectangle[] missileBulletUp;


		public SamusBullet(Character c) : base(c, "samus_bullet"){
			player = (Samus) owner;
			hitBox = new Rectangle(0, 0, 8, 10);
			maxDistance = 20;
            normalBullet = new Rectangle[]{new Rectangle(184,1352,8,10), new Rectangle(196,1348,12,12)};
            missileBullet = new Rectangle[]{new Rectangle(158,1418,22,16), new Rectangle(360,1414,20,20)};
            missileBulletUp = new Rectangle[]{new Rectangle(184,1410,16,24), new Rectangle(360,1414,20,20)};
            spritePos = normalBullet;
			speedX = 5.5f;
			speedY = 5.5f;
			damage = 1;
		}


		public override bool Spawn(){
			if(!active && player.bulletDelay<=0.0f){
				active = true;
				actualDistance = 0.0f;
                if(player.equipedMissile){
                    missile = true;
                    player.bulletDelay = 50.0f;
                    damage = 5;
                    if(!player.aimUp){
                        spritePos = missileBullet;
                    } else {
                        spritePos = missileBulletUp;
                    }
                } else {
                    missile = false;
                    player.bulletDelay = 15.0f;
                    damage = 1;
                    spritePos = normalBullet;
                }

				
				if(player.aimUp) {
					vDirection = -1;
					direction = 0;
					if (player.direction == 1) {
						position = player.position + new Vector2(16,-12);
					} else {
						position = player.position + new Vector2(8,-12);
					}

				} else {
					vDirection = 0;
					direction = player.direction;
					if (player.direction == 1) {
						position = player.position + new Vector2 (32, 12);
					} else {
						position = player.position + new Vector2 (-8, 12);
					}
				}
				return true;
			}
			return false;
		}

        public override void Sound(){
            if(!player.equipedMissile){
                game.sfxShoot.Play();
            } else {
                game.sfxMissile.Play();
            }
        }


        public override bool Despawn(bool hit = true){
            base.Despawn(hit);
            if(hit){
                if(lastBlockDetected != null && lastBlockDetected.GetType() == typeof(DoorBlock)){ 
                    DoorBlock b = (DoorBlock)lastBlockDetected;
                    if(!b.isDoorFrame && !b.open){
                        if(b.door.normal){
                            b.Open();
                        } else if(missile && b.door.missileHits > 0){
                            b.door.missileHits--;
                            if(b.door.missileHits == 0){
                                b.Open();
                            }
                        } else {
                            game.sfxDamageRipper.Play();
                        }
                    }
                } else if(lastBlockDetected != null && lastBlockDetected.breakable){
                    lastBlockDetected.destroy();
                }
            }
            return true;
        }	
			
	}
}

