﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace metroid{
	
	public class Bullet : Entity {

		protected Character owner;
		public int vDirection = 1;
		public bool active = false;
		public float speed = 1.0f;
		public float speedX = 1.0f;
		public float speedY = 1.0f;
		public float maxDistance = 1.0f;
		public float actualDistance = 0.0f;
		public float despawnTimer = 0.0f;
		public Rectangle[] spritePos;


		public Bullet(Character c, string n) : base (c.room,n){
			owner = c;
			active = false;
			blockSize = 1;
			width = 1;
			height = 1;
			orientation = Orientation.Horizontal;
			speedX = speedY;
            damage = 5;
		}

		public virtual bool Spawn(){
			return !active;
		}

		public virtual bool Despawn(bool hit=true){
			if(hit) {
				despawnTimer = 8.0f;
			} else {
                despawnTimer = 0.0f;
                actualDistance = 0;
				active = false;
			}
			return true;
		}

		public override void Update(GameTime gameTime){
			if(active) {
				if(despawnTimer <= 0.0f) {
					actualDistance += speed;
					if(actualDistance <= maxDistance) {
                        room = owner.room;
						if(DetectCollision()) {
							Despawn();
						} else {
							position.X += speedX * direction;
							position.Y += speedY * vDirection;
						}
					} else {
						Despawn(false);
					}
				} else {
					despawnTimer -= gameTime.ElapsedGameTime.Milliseconds/10.0f;
					if(despawnTimer <= 0.0f) {
						despawnTimer = 0.0f;
                        actualDistance = 0;
						active = false;
					}
				}

			}
		}

		public override void Draw(GameTime gameTime){
			if(active) {
                //Console.WriteLine(position.X);
                SpriteEffects se = SpriteEffects.None;
                if(owner.direction == -1){
                    se = SpriteEffects.FlipHorizontally;
                }
				if(despawnTimer <= 0.0f) {
					game.spriteBatch.Draw(
						owner.spriteSheet,
						new Vector2(
							position.X - owner.room.cameraPos.X,
							position.Y - owner.room.cameraPos.Y
						),
						spritePos[0],
						Color.White,
						0.0f,
						new Vector2(0, 0),
						1.0f,
						se,
						0.75f
					);
				} else {
					if(spritePos.Length > 1) {
						game.spriteBatch.Draw(
							owner.spriteSheet,
							new Vector2(
								position.X - owner.room.cameraPos.X,
								position.Y - owner.room.cameraPos.Y
							),
							spritePos[1],
							Color.White,
							0.0f,
							new Vector2(0, 0),
							1.0f,
							SpriteEffects.None,
							0.75f
						);
					} else {
						game.spriteBatch.Draw(
							owner.spriteSheet,
							new Vector2(
								position.X - owner.room.cameraPos.X,
								position.Y - owner.room.cameraPos.Y
							),
							spritePos[0],
							Color.White,
							0.0f,
							new Vector2(0, 0),
							1.0f,
							SpriteEffects.None,
							0.75f
						);
					}
				}
			}
		}
	}
}

