﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace metroid{
	
	public class Samus : Character{

		public bool aimUp = false;
		public bool shooting = false;
		public bool longShot = false;
		public bool maruMari = false;
		public bool highJump = false;
		public bool equipedMissile = false;
        public int maxEnergyTanks = 0;
        public int fullEnergyTanks = 0;
		public int maxMissiles = 0;
        public int missiles = 0;
		public Rectangle baseHitBox;
		public Rectangle ballHitBox;
		public Rectangle jumpHitBox;
		public float damageTimer = 0.0f;
		public float bulletDelay = 0.0f;
        public Vector2 damageVector;
        public int startHp = 35;
        public bool onLava = false;
        public bool dead = false;
        public float deathTimer = 0f;
        public int deathAnimationPosY = 0;
        public float deathAnimationTimer = 8f;


		public Samus(Room r): base(r,"Samus"){
			orientation = Orientation.Vertical;
			blockSize = 2;
			width = 32;
			height = 64;
			baseHitBox = new Rectangle(6, 0, 20, 60);
			ballHitBox = new Rectangle(6, 0, 20, 26);
			jumpHitBox = new Rectangle(6,10,20,50);
			hitBox = baseHitBox;
			position = new Vector2(0, 0);
			direction = 1;
			spriteSheet = game.samusSprites;
			speedX = 2.5f;
			speedY = 2.5f;
			baseHp = 99;
            hp = startHp;

			bullets = new List<Bullet>(){new SamusBullet(this),new SamusBullet(this),new SamusBullet(this)};


			states = new StateMachine(new Dictionary<string, BaseState>(){
				{"spawn", new SpawnState(game,"spawn",this)},
				{"idle", new IdleState(game,"idle",this)},
				{"walk", new WalkState(game,"walk",this)},
				{"jump", new JumpState(game,"jump",this)},
				{"fall", new FallState(game,"fall",this)},
				{"ball", new BallState(game,"ball",this)}
			},"idle");

			animations = new Dictionary<string,Animation[]>() {
				{"idle", new Animation[]{
						new Animation(new Vector2(0,-2),new Rectangle(145,12,42,64),1f)
					}
				},
				{"idle_shoot", new Animation[]{
						new Animation(new Vector2(0,-2),new Rectangle(638,12,36,64),1f)
					}
				},
				{"up", new Animation[]{
						new Animation(new Vector2(1,-14),new Rectangle(190,0,28,77),1f)
					}
				},
				{"up_shoot", new Animation[]{
						new Animation(new Vector2(1,-14),new Rectangle(878,4,28,72),1f)
					}
				},
				{"walk", new Animation[]{
						//new Animation(new Vector2(-4,0),new Rectangle(145,12,40,64),5.5f),
						new Animation(new Vector2(-4,-1),new Rectangle(222,14,30,62),6.0f),
						new Animation(new Vector2(-6,-2),new Rectangle(256,12,34,64),6.0f),
						new Animation(new Vector2(-12,-2),new Rectangle(294,12,40,64),6.0f)
					}	
				},
				{"walk_shoot", new Animation[]{
						new Animation(new Vector2(-4,-1),new Rectangle(682,14,40,62),6.0f),
						new Animation(new Vector2(-6,-2),new Rectangle(726,14,44,62),6.0f),
						new Animation(new Vector2(-12,-2),new Rectangle(774,14,50,62),6.0f)
					}	
				},
				{"walk_up", new Animation[]{
						new Animation(new Vector2(-4,-14),new Rectangle(910,0,24,76),6.0f),
						new Animation(new Vector2(-8,-14),new Rectangle(944,0,34,76),6.0f),
						new Animation(new Vector2(-15,-14),new Rectangle(982,0,38,76),6.0f)
					}	
				},
				{"jump", new Animation[]{
						new Animation(new Vector2(-2,9),new Rectangle(338,26,36,50),4.0f),
						new Animation(new Vector2(-2,9),new Rectangle(338,26,36,50),4.0f),
						new Animation(new Vector2(-2,9),new Rectangle(338,26,36,50),4.0f),
						new Animation(new Vector2(-2,9),new Rectangle(338,26,36,50),4.0f),
						new Animation(new Vector2(-2,9),new Rectangle(338,26,36,50),4.0f)
					}	
				},
				{"jump_shoot", new Animation[]{
						new Animation(new Vector2(-2,9),new Rectangle(828,26,46,50),4.0f),
						new Animation(new Vector2(-2,9),new Rectangle(828,26,46,50),4.0f),
						new Animation(new Vector2(-2,9),new Rectangle(828,26,46,50),4.0f),
						new Animation(new Vector2(-2,9),new Rectangle(828,26,46,50),4.0f),
						new Animation(new Vector2(-2,9),new Rectangle(828,26,46,50),4.0f)
					}	
				},
				{"jump_up", new Animation[]{
						new Animation(new Vector2(-2,9),new Rectangle(1024,12,36,64),4.0f),
						new Animation(new Vector2(-2,9),new Rectangle(1024,12,36,64),4.0f),
						new Animation(new Vector2(-2,9),new Rectangle(1024,12,36,64),4.0f),
						new Animation(new Vector2(-2,9),new Rectangle(1024,12,36,64),4.0f),
						new Animation(new Vector2(-2,9),new Rectangle(1024,12,36,64),4.0f)
					}	
				},
				{"jump_up_shoot", new Animation[]{
						new Animation(new Vector2(-2,9),new Rectangle(1064,16,36,60),4.0f),
						new Animation(new Vector2(-2,9),new Rectangle(1064,16,36,60),4.0f),
						new Animation(new Vector2(-2,9),new Rectangle(1064,16,36,60),4.0f),
						new Animation(new Vector2(-2,9),new Rectangle(1064,16,36,60),4.0f),
						new Animation(new Vector2(-2,9),new Rectangle(1064,16,36,60),4.0f)
					}	
				},
				{"jump_roll", new Animation[]{
						new Animation(new Vector2(-2, 9),new Rectangle(338,26,36,50),16.0f,true),
						new Animation(new Vector2( 2, 9),new Rectangle(378,30,28,46),4.0f),
						new Animation(new Vector2(-3,15),new Rectangle(410,46,38,30),4.0f),
						new Animation(new Vector2( 2, 9),new Rectangle(452,30,28,46),4.0f),
						new Animation(new Vector2(-3,15),new Rectangle(484,46,38,30),4.0f)
					}	
				},
				{"ball", new Animation[]{
						new Animation(new Vector2(3,0),new Rectangle(526,50,24,26),6.0f),
						new Animation(new Vector2(3,1),new Rectangle(554,50,24,26),6.0f),
						new Animation(new Vector2(3,0),new Rectangle(582,50,24,26),6.0f),
						new Animation(new Vector2(3,1),new Rectangle(610,50,24,26),6.0f)
					}	
				},
				{ "spawn", new Animation[]{
                        new Animation(new Vector2( 0,0),new Rectangle(0,0,1,1),     80f),//80
						new Animation(new Vector2( 0,0),new Rectangle(2,12,32,64),  10f),//160
                        new Animation(new Vector2( 0,0),new Rectangle(0,0,1,1),     10f),//220
                        new Animation(new Vector2( 0,0),new Rectangle(2,12,32,64),  10f),//160
                        new Animation(new Vector2( 0,0),new Rectangle(0,0,1,1),     10f),//220
                        new Animation(new Vector2( 0,0),new Rectangle(2,12,32,64),  10f),//160
                        new Animation(new Vector2( 0,0),new Rectangle(0,0,1,1),     10f),//220
                        new Animation(new Vector2( 0,0),new Rectangle(2,12,32,64),  10f),//160
                        new Animation(new Vector2( 0,0),new Rectangle(0,0,1,1),     10f),//220
                        new Animation(new Vector2( 0,0),new Rectangle(2,12,32,64),  10f),//160
                        new Animation(new Vector2( 0,0),new Rectangle(0,0,1,1),     10f),//220
                        new Animation(new Vector2( 0,0),new Rectangle(2,12,32,64),  10f),//160
                        new Animation(new Vector2( 0,0),new Rectangle(0,0,1,1),     10f),//220
                        new Animation(new Vector2( 0,0),new Rectangle(2,12,32,64),  10f),//160
                        new Animation(new Vector2( 0,0),new Rectangle(0,0,1,1),     10f),//220
						
                        new Animation(new Vector2( 0,0),new Rectangle(38,12,32,64), 10f),//300
                        new Animation(new Vector2( 0,0),new Rectangle(0,0,1,1),     10f),//360
                        new Animation(new Vector2( 0,0),new Rectangle(38,12,32,64), 10f),//300
                        new Animation(new Vector2( 0,0),new Rectangle(0,0,1,1),     10f),//360
                        new Animation(new Vector2( 0,0),new Rectangle(38,12,32,64), 10f),//300
                        new Animation(new Vector2( 0,0),new Rectangle(0,0,1,1),     10f),//360
                        new Animation(new Vector2( 0,0),new Rectangle(38,12,32,64), 10f),//300
                        new Animation(new Vector2( 0,0),new Rectangle(0,0,1,1),     10f),//360
                        new Animation(new Vector2( 0,0),new Rectangle(38,12,32,64), 10f),//300
                        new Animation(new Vector2( 0,0),new Rectangle(0,0,1,1),     10f),//360
                        new Animation(new Vector2( 0,0),new Rectangle(38,12,32,64), 10f),//300
                        new Animation(new Vector2( 0,0),new Rectangle(0,0,1,1),     10f),//360
                        new Animation(new Vector2( 0,0),new Rectangle(38,12,32,64), 10f),//300
                        new Animation(new Vector2( 0,0),new Rectangle(0,0,1,1),     10f),//360
                        new Animation(new Vector2( 0,0),new Rectangle(38,12,32,64), 10f),//300
                        new Animation(new Vector2( 0,0),new Rectangle(0,0,1,1),     10f),//360

                        new Animation(new Vector2( 0,0),new Rectangle(74,12,32,64), 10f),//440
                        new Animation(new Vector2( 0,0),new Rectangle(0,0,1,1),     10f),//500
                        new Animation(new Vector2( 0,0),new Rectangle(74,12,32,64), 10f),//440
                        new Animation(new Vector2( 0,0),new Rectangle(0,0,1,1),     10f),//500
                        new Animation(new Vector2( 0,0),new Rectangle(74,12,32,64), 10f),//440
                        new Animation(new Vector2( 0,0),new Rectangle(0,0,1,1),     10f),//500
                        new Animation(new Vector2( 0,0),new Rectangle(74,12,32,64), 10f),//440
                        new Animation(new Vector2( 0,0),new Rectangle(0,0,1,1),     10f),//500
                        new Animation(new Vector2( 0,0),new Rectangle(74,12,32,64), 10f),//440
                        new Animation(new Vector2( 0,0),new Rectangle(0,0,1,1),     10f),//500
                        new Animation(new Vector2( 0,0),new Rectangle(74,12,32,64), 10f),//440
                        new Animation(new Vector2( 0,0),new Rectangle(0,0,1,1),     10f),//500
                        new Animation(new Vector2( 0,0),new Rectangle(74,12,32,64), 10f),//440
                        new Animation(new Vector2( 0,0),new Rectangle(0,0,1,1),     10f),//500
						
                        new Animation(new Vector2( 0,0),new Rectangle(0,0,1,1),     10f),//500
                        new Animation(new Vector2( 0,0),new Rectangle(110,12,32,64),10f),//
                        new Animation(new Vector2( 0,0),new Rectangle(0,0,1,1),     10f),//500
                        new Animation(new Vector2( 0,0),new Rectangle(110,12,32,64),10f),//
                        new Animation(new Vector2( 0,0),new Rectangle(0,0,1,1),     10f),//500
                        new Animation(new Vector2( 0,0),new Rectangle(110,12,32,64),10f),//
                        new Animation(new Vector2( 0,0),new Rectangle(0,0,1,1),     10f),//500
                        new Animation(new Vector2( 0,0),new Rectangle(110,12,32,64),10f),//

                        new Animation(new Vector2( 0,0),new Rectangle(110,12,32,64),160f)//
						
					} 
				},
			};
		}

        public void SetHp(int val){
            hp += val;
            if(hp > 99){
                if(maxEnergyTanks > 0){
                    if(fullEnergyTanks == maxEnergyTanks){
                        hp = 99;
                    } else {
                        fullEnergyTanks++;
                        hp -= 99;
                    }
                } else {
                    hp = 99;
                }
            } else if(hp <= 0){
                if(maxEnergyTanks > 0){
                    if(fullEnergyTanks == 0){
                        Die();
                    } else {
                        fullEnergyTanks--;
                        hp += 99;
                    }
                } else {
                    hp = 0;
                    Die();
                }
            }

        }

        public void Spawn(){
            hp = 35;
            fullEnergyTanks = 0;
            missiles = maxMissiles;
        }

        public override void Die(){
            game.music.Stop();
            game.sfxDeath.Play();
            dead = true;
            deathTimer = 18f * 8f;
            deathAnimationTimer = 8f;
        }
			
		
		public override void Update(GameTime gameTime){
            if(!dead){
                states.activeState.Update(gameTime);
            }
		}

		public override void Draw(GameTime gameTime){
            if(!dead){
                base.Draw(gameTime);
                if(game.debug) {
                    game.spriteBatch.Draw(
                        spriteSheet,
                        new Vector2(
                            position.X - room.cameraPos.X,
                            position.Y - room.cameraPos.Y - 160
                        ),
                        new Rectangle(0, 728, 32, 224),
                        Color.White,
                        0.0f,
                        new Vector2(0, 0),
                        1.0f,
                        SpriteEffects.None,
                        1.0f

                    );

			    }
                if(game.borderDebug) {
                    game.spriteBatch.Draw(
                        spriteSheet,
                        new Vector2(
                            position.X - room.cameraPos.X,
                            position.Y - room.cameraPos.Y
                        ),
                        new Rectangle(0, 964, width, height),
                        Color.White,
                        0.0f,
                        new Vector2(0, 0),
                        1.0f,
                        SpriteEffects.None,
                        0.9f

                    );
			    }
            } else {
                if(deathTimer > 0f){
                    deathTimer -= gameTime.ElapsedGameTime.Milliseconds / 10f;
                    game.spriteBatch.Draw(
                        game.samusDeathSprites,
                        new Vector2(
                            position.X - room.cameraPos.X-96,
                            position.Y - room.cameraPos.Y-60
                        ),
                        new Rectangle(0, deathAnimationPosY, 224, 124),
                        Color.White,
                        0.0f,
                        new Vector2(0, 0),
                        1.0f,
                        SpriteEffects.None,
                        0.9f

                    );
                    deathAnimationTimer-= gameTime.ElapsedGameTime.Milliseconds / 10f;
                    if(deathAnimationTimer <= 0f){
                        deathAnimationTimer = 8f;
                        deathAnimationPosY += 124;
                    }
                    if(deathTimer <= 0f){
                        deathTimer = 0f;
                        game.gameState="over";
                    }
                }
            }
		}
	}
}

