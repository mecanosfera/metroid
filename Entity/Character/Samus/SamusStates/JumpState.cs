﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace metroid {
	
	public class JumpState : SamusState {


		public bool roll = false;
		public float startPositionY;
		public float jumpTime = 0.0f;
		public float jumpSpeed = 0.0f;
		public bool released = false;

		public JumpState(Game1 g, string n, Character c) : base(g,n,c) {
			
		}

		public override void Enter(string lastState=null){
			animationType="jump";
			roll = false;
			released = false;
			if(lastState == "walk") {
				animationType="jump_roll";
				roll = true;
			}
			if (self.aimUp){		
				if (!self.shooting){
					animationType = "jump_up";
				} else {
					animationType = "jump_up_shoot";
				}
			} else {
				if (self.shooting){
					animationType = "jump_shoot";
				}
			}
			animationIndex = 0;
			animationTimer = 0.0f;
			acel = 0.19f;
			startPositionY = character.position.Y-1;
			jumpTime = 0.0f;
			self.hitBox = self.jumpHitBox;
            game.sfxJump.Play();
            walkSpeed = 2.5f;
		}

		public override void HandleInput(GameTime gameTime){
			Vector2 dir = character.position;

			if(Key(Game1.RIGHT)) {
				Right();
			}

			if(Key(Game1.LEFT)) {
				Left();
			}

			if (KeyUp(Game1.JUMP) && !released) {
				released = true;
				jumpTime = (float)Math.Max(jumpTime,Math.PI/2);
			}
				
			jumpSpeed = (float)Math.Cos (jumpTime) * 5.5f;
			jumpTime += (float)gameTime.ElapsedGameTime.TotalSeconds * 2.0f;
			if (jumpTime <= 0f){
				character.states.Change("fall");
			}
				

			character.position.Y -= jumpSpeed;
			if(character.DetectCollision()) {
				character.position.Y += jumpSpeed;
				character.states.Change("fall");
				return;
			}

			if (character.position.Y >= startPositionY){
				character.states.Change("fall");
			}	

			if(self.aimUp) {
				Shoot("jump_up_shoot","jump_up");
			} else {
				Shoot("jump_shoot","jump");
			}

			if (self.shooting){
				AimUp("jump_up_shoot", "jump_shoot");
			} else {
				AimUp("jump_up", "jump");	
			}


		}

	}
}

