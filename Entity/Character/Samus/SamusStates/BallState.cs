﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace metroid {
	
	public class BallState : SamusState{

	

		public BallState(Game1 g, string n, Character c) : base(g,n,c) {
			
		}

		public override void Enter(string lastState=null){
			animationType="ball";
			animationIndex = 0;
			animationTimer = 0.0f;
			character.position.Y += 16;
			character.hitBox = ((Samus)character).ballHitBox;
			character.blockSize = 1;
			character.height = 32;
		}

		public override void HandleInput(GameTime gameTime){
			Vector2 dir = character.position;

			if(Key(Game1.RIGHT)) {
				Right();
			}

			if(Key(Game1.LEFT)) {
				Left();
			}

			if(Key(Game1.UP)) {
				character.position.Y -= 34;
				if(character.DetectCollision()) {
					character.position.Y = dir.Y;
				} else {
					character.states.Change("idle");
					return;
				}
			}

			Gravity();
		}


		public override void Exit(string newState=null){
			character.hitBox = ((Samus)character).baseHitBox;
			character.blockSize = 2;
			character.height = 64;
		}
	}
}

