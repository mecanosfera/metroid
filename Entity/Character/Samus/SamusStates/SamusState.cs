﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace metroid {
	
	public class SamusState : CharacterState {


		protected float gravity = 3.75f;
		protected float baseWalkSpeed = 3.25f;
		protected float baseJumpSpeed = 5.75f;
		protected float fallSpeed = 0f;
		protected float acel = 0.1f;
		protected Samus self;
        protected bool stopMoveDamage = true;
        protected bool drawDamageFlip = true;

		public SamusState(Game1 g, string n, Character c) : base(g,n,c) {
			walkSpeed = baseWalkSpeed;
			self = (Samus)character;
		}

        public bool OnLava(){
            int[][] blocks = self.OverlapingBlocks();
            Room room = self.room;
            for(int i = 0; i < blocks.Length; i++) {
                if(blocks[i][0] <= room.width && blocks[i][1] <= room.height){
                    Block block = room.blocks[blocks[i][0], blocks[i][1]];
                    if(block != null && block.lava>-1) {
                        if(self.TestCollision(block)) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }


		public override void Update(GameTime gameTime) {
			if(!self.room.world.transition) {
				base.Update(gameTime);
                if(game.Select()){
                    if(self.maxMissiles > 0){
                        self.equipedMissile = !self.equipedMissile;
                    }
                }
                self.onLava = OnLava();
                float seconds = (float) gameTime.ElapsedGameTime.TotalSeconds;
				float time = gameTime.ElapsedGameTime.Milliseconds / 10.0f;
				if(self.bulletDelay > 0.0f) {
					self.bulletDelay -= time;
					if(self.bulletDelay < 0.0f) {
						self.bulletDelay = 0.0f;
					}
				}
                //Console.WriteLine(self.damageTimer);
				if(self.damageTimer <= 0.0f) {
                    if(self.onLava){
                        TakeDamage(null);
                    }
					if(self.room.enemies.Count > 0) {
						foreach(Enemy e in self.room.enemies) {
							if (!e.deadState) {
								if (self.TestCollision(e)) {
									TakeDamage(e);
									//return;
								}
							}
						}
                        foreach(Bullet b in self.room.enemyBullets){
                            if(b.active && self.TestCollision(b)){
                                TakeDamage(b);
                            }
                        }
					}
				} else {
					self.damageTimer -= seconds;
                    if(self.states.activeState.name == "fall" || self.states.activeState.name=="ball"){
                        MoveX(self.damageVector.X);
                        MoveY(self.damageVector.Y, -1);
                    }
					if(self.damageTimer < 0) {
						self.damageTimer = 0.0f;
                        stopMoveDamage = true;
					}
				}
				HandleInput(gameTime);
			}
		}

        public virtual void TakeDamage(Entity e){
            if(self.damageTimer <= 0f && !game.invulnerable){
                game.sfxDamageSamus.Play();
                if(!self.onLava){
                    self.damageTimer = 1f;
                } else {
                    self.damageTimer = 0.5f;
                }
                if(e != null){
                    self.SetHp(e.damage * -1);
                    if(!self.onLava){
                        if(e.position.X > self.position.X){
                            self.damageVector.X = -1;
                        } else {
                            self.damageVector.X = 1;
                        }
                        self.damageVector.Y = 3;
                    }
                    if(!self.onLava && self.states.activeState.name != "fall"){
                        if(self.states.activeState.name != "ball"){
                            self.states.Change("fall");
                        }
                    }
                } else {
                    self.SetHp(-1);
                    self.damageVector.X = 0;
                    self.damageVector.Y = 0;
                }
            }
            //self.damageVector = new Vector2(1 * e.direction, 1);
		}


			

		public virtual bool Key(Keys k){
			return Keyboard.GetState().IsKeyDown(k);
		}

		public virtual bool KeyUp(Keys k){
			return Keyboard.GetState().IsKeyUp(k);
		}

		protected override bool MoveY(float speed, int direction=1){
			if(!self.room.world.transition){
				return base.MoveY(speed, direction);
			}
			return false;
		}


		protected virtual bool Gravity(){
			if(!MoveY(gravity)) {
				return true;
			}
			return false;
		}


		protected override bool MoveX(float speed){
			if(!self.room.world.transition) {
				if(speed == 0.0f) {
					speed = walkSpeed;
				}
                if(self.onLava){
                    speed -= 1.75f;
                }
				character.position.X += speed * character.direction;
				bool collision = character.DetectCollision();
                if(collision && self.lastBlockDetected.GetType() == typeof(DoorBlock)) {
                    if(((DoorBlock)self.lastBlockDetected).isDoorFrame) {
                        //Console.WriteLine(self.room.name);
                        self.room.Transition(((DoorBlock)self.lastBlockDetected).door);
                        collision = false;
                    } else {
                        if(!((DoorBlock)self.lastBlockDetected).open){
                            collision = true;
                        } else {
                            collision = false;
                        }
                    }
                }
				if(collision) {
                    character.position.X -= speed * character.direction;
                    float sub = 0.1f;
                    while(speed >= 0) {
                        speed -= sub; 
                        if(speed < 0) {
                            sub = speed * -1;
					    }
                        character.position.X += sub * character.direction;
                        if(character.DetectCollision()) {
                            character.position.X -= sub * character.direction;
                            return false;
					    }
				    }
				}
				return true;
			}
			return false;
		}


		protected virtual bool Right(float speed=0.0f){
			if(character.direction == -1) {
				character.direction = 1;
			} else {
				return MoveX(speed);
			}
			return false;
		}

		protected virtual bool Left(float speed=0.0f){
			if(character.direction == 1) {
				character.direction = -1;
			} else {
				return MoveX(speed);
			}
			return false;
		}


		protected virtual bool AimUp(string animUp, string animDefault){
            if(game.Pressed("UP")) {
				if(!self.aimUp) {
					self.aimUp = true;
					animationType = animUp;
				
				}
				return false;
			} else {//if (KeyUp(Game1.UP)) {
				//Console.WriteLine("rughhhhhhhhhhhhhhhhhh");
				if(self.aimUp) {
					self.aimUp = false;
					animationType = animDefault;
				}
			}
			return false;
		}
			

		protected virtual bool Shoot(string animShoot, string animDefault){
            if(game.Pressed("SHOOT")) {
                if(!self.equipedMissile){
                    if(!self.shooting) {
                        self.shooting = true;
                        animationType = animShoot;
				    }
                    foreach(Bullet b in self.bullets) {
                        if(b.Spawn()) {
                            b.Sound();
                            return true;
					    }
				    }
                    return true;
                } else {
                    if(self.missiles > 0){
                        if(!self.shooting){
                            self.shooting = true;
                            animationType = animShoot;
                        }
                        foreach(Bullet b in self.bullets) {
                            if(b.Spawn()) {
                                self.missiles -= 1;
                                b.Sound();
                                return true;
                            }
                        }
                        return true;
                    }
                    return false;
                }
            } else if (game.Released("SHOOT")) {
				if(self.shooting) {
					self.shooting = false;
					animationType = animDefault;
				}
			}
			return false;
		}

		protected virtual bool JumpStart(float init){
			character.position.Y += -2.0f;
			if(character.DetectCollision()) {
				character.position.Y = init;
				return false;
			}
			return true;
		}



        public override void Draw(GameTime gameTime){
            if(drawDamageFlip){

                Animation sprite;
                float time = gameTime.ElapsedGameTime.Milliseconds/10.0f;
                int moreY = 0;
                if(self.equipedMissile){
                    moreY = 94;
                }

                //Console.WriteLine(time+","+animationTimer);
                if(debug) {
                    sprite = new Animation(new Vector2(character.hitBox.X, character.hitBox.Y), new Rectangle(0, 440, character.hitBox.Width, character.hitBox.Height), 1f);
                } else {
                    if(!character.room.world.paused && !self.room.getItem){
                        animationTimer -= time;
                        if(animationTimer <= 0.0f) {
                            sprite = nextSprite(); 
                            animationTimer = sprite.time;
                        } else {
                            sprite = character.animations[animationType][animationIndex];
                        }
                    } else {
                        sprite = character.animations[animationType][animationIndex];
                    }
                }
                Rectangle drawArea = new Rectangle(sprite.drawArea.X, sprite.drawArea.Y+moreY,sprite.drawArea.Width,sprite.drawArea.Height);

                SpriteEffects se = SpriteEffects.None;
                float spritePosX = sprite.sheetPos.X;
                if(flipAnimation && character.direction == -1) {
                    se = SpriteEffects.FlipHorizontally;
                    spritePosX = character.width-(sprite.drawArea.Width + sprite.sheetPos.X);
                }

                game.spriteBatch.Draw(
                    character.spriteSheet,
                    new Vector2(
                        character.position.X-character.room.cameraPos.X+spritePosX,
                        character.position.Y-character.room.cameraPos.Y+sprite.sheetPos.Y
                    ),
                    drawArea,
                    Color.White,
                    0.0f,
                    new Vector2(0,0),
                    1.0f,
                    se,
                    0.75f

                );

                foreach(Bullet b in character.bullets) {
                    if(b.active) {
                        b.Draw(gameTime);
                    }
                }


                if(self.damageTimer > 0f){
                    drawDamageFlip = false;
                }
            } else {
                drawDamageFlip = true;
            }
        }
	}
}

