﻿using System;

namespace metroid{
    
    public class SpawnState :SamusState {

        float timer = 42f;


        public SpawnState(Game1 g, string n, Character c) : base(g,n,c){
            
		}

        public override void Enter(string lastState){
            animationType = "spawn";    
            timer = 660f;
        }


        public override void Update(Microsoft.Xna.Framework.GameTime gameTime){
            if(timer>0f){
                timer -= gameTime.ElapsedGameTime.Milliseconds / 10f;
                if(timer <= 0f){
                    self.room.Spawn(true);
                    self.states.Change("idle");
                }
            }
        }

	}
}

