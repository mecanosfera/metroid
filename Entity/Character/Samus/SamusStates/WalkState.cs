﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace metroid {
	
	public class WalkState : SamusState {


		public WalkState(Game1 g, string n, Character c) : base(g,n,c) {
			animationType = "walk";			
		}

		public override void Enter(string lastState=null){
			animationType="walk";
			if (self.aimUp){				
				animationType = "walk_up";
			} else {
				if (self.shooting){
					animationType = "walk_shoot";
				}
			}
			animationIndex = 0;
			animationTimer = 0.0f;

		}
			
		public override void HandleInput(GameTime gameTime){
			Vector2 dir = character.position;

			if(Key(Game1.RIGHT)) {
				if(!Right()) {
					character.states.Change("idle");
					return;
				}
			}

			if(Key(Game1.LEFT)) {
				if(!Left()) {
					character.states.Change("idle");
					return;
				}
			}


			if(Key(Game1.JUMP) && ((IdleState)character.states.get("idle")).allowJump) {
				if(JumpStart(dir.Y)) {
					character.states.Change("jump");
					return;
				}
			}

			if(KeyUp(Game1.JUMP)) {
				((IdleState)character.states.get("idle")).allowJump = true;
			}

			if(!Gravity()) {
				character.states.Change("fall");
				return;
			}

			if(KeyUp(Game1.RIGHT) && KeyUp(Game1.LEFT)) {
				character.states.Change("idle");
				return;
			}



			if(self.aimUp) {
				Shoot("walk_up","walk_up");
			} else {
				Shoot("walk_shoot","walk");
			}

			if (self.shooting){
				AimUp("walk_up", "walk_shoot");
			} else {
				AimUp("walk_up", "walk");	
			}



			/*if (Keyboard.GetState().IsKeyDown(Keys.Down))
				character.position.Y += 1.0f*character.speedY;
			if(character.DetectCollision()) {
				character.position.Y = dir.Y;
			}*/
		}

			

	}
}

