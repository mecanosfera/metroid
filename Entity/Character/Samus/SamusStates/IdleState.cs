﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace metroid {
	public class IdleState : SamusState{


		public bool allowJump = true;



		public IdleState(Game1 g, string n, Character c) : base(g,n,c){
			animationType = "idle";
			if(self.aimUp) {
				animationType = "up";
			}
		}

		public override void Enter(string lastState=null){
			animationType="idle";
			if(self.aimUp) {
				animationType = "up";
			}
			animationIndex = 0;
			animationTimer = 0.0f;
			if(Key(Game1.JUMP)) {
				allowJump = false;
			}
		}

		public override void HandleInput(GameTime gameTime){
			Vector2 dir = character.position;

			if(!allowJump && KeyUp(Game1.JUMP)) {
				allowJump = true;
			}

			if(self.aimUp) {
				Shoot("up","up");
			} else {
				Shoot("idle","idle");
			}
				
			AimUp("up", "idle");



			//RIGHT
			if(Key(Game1.RIGHT)) {
				if(Right()) {
					character.states.Change("walk");
					return;
				}
			}

			if(Key(Game1.LEFT)) {
				if(Left()) {
					character.states.Change("walk");
					return;
				}
			}

			if(allowJump && Key(Game1.JUMP)) {
				if(JumpStart(dir.Y)) {
					character.states.Change("jump");
					return;
				}
			}


			if(Keyboard.GetState().IsKeyDown(Keys.Down)) {
				if(((Samus)character).maruMari) {
					character.states.Change("ball");
					return;
				}
			}

			if(!Gravity()) {
				character.states.Change("fall");
				return;
			}

		}
	}
}

