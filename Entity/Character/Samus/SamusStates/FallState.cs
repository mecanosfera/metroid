﻿using System;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace metroid{
	
	public class FallState : SamusState{
	

		float endRollPosition;
		


		public FallState(Game1 g, string n, Character c) : base(g,n,c) {
			animationType = "idle";


		}

		public override void Enter(string lastState=null){
			animationType = "jump";
			animationIndex = 0;
			animationTimer = 0.0f;
			fallSpeed = 0.0f;
			if(lastState == "jump" && ((JumpState) character.states.get(lastState)).roll) {
				animationType="jump_roll";
				endRollPosition = ((JumpState)character.states.get(lastState)).startPositionY;
				fallSpeed = ((JumpState)character.states.get(lastState)).jumpSpeed;
				if (fallSpeed < 0) {
					fallSpeed *= -1;
				}
                if(fallSpeed > 2f && character.position.Y<endRollPosition){
                    fallSpeed = 0f;
                }
			}
            if (self.aimUp){        
                if (!self.shooting){
                    animationType = "jump_up";
                } else {
                    animationType = "jump_up_shoot";
                }
            } else {
                if (self.shooting){
                    animationType = "jump_shoot";
                }
            }
            gravity = 8.0f;
			acel = 0.1f;

			self.hitBox = self.jumpHitBox;
		}

	
		public override void HandleInput(GameTime gameTime){
			if(fallSpeed <= gravity) {
				fallSpeed += acel;
			}
			if(!MoveY(fallSpeed)){
				character.states.Change("idle");
			} else if (animationType == "jump_roll" && character.position.Y > endRollPosition){
				animationType = "jump";
				animationIndex = 0;
				animationTimer = 0.0f;
			}

           

			if(Key(Game1.RIGHT)) {
				Right();
			}

			if(Key(Game1.LEFT)) {
				Left();
			}

            if(self.aimUp) {
                Shoot("jump_up_shoot","jump_up");
            } else {
                Shoot("jump_shoot","jump");
            }

            if (self.shooting){
                AimUp("jump_up_shoot", "jump_shoot");
            } else {
                AimUp("jump_up", "jump");   
            }
				 
		}

		public override void Exit(string newState=null){
			self.hitBox = self.baseHitBox;
		}
	}
}

