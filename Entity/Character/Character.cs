﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace metroid{
	
	public abstract class Character : Entity{

		public float speedX = 1.0f;
		public float speedY = 1.0f;
		public int baseHp;
		public int hp;
		public int rotation = 0; //1 top, 2 right, 3 bottom, 3 left
		public int baseRotation;
		public int baseDirection;
		public int maxBullets = 3;
		public float maxDistBullet = 4*32;
		public List<Bullet> bullets = new List<Bullet>();
		
		public Character(Room r, string n): base(r,n){
			
		}

		public virtual bool Damage(){
			return false;
		}

		public virtual void Die(){}

		public override void Draw(GameTime gameTime){
			states.activeState.Draw(gameTime);
			if(game.borderDebug) {
				Animation sprite = new Animation(new Vector2(hitBox.X, hitBox.Y), new Rectangle(0, 440, hitBox.Width, hitBox.Height), 1f);
				float spritePosX = sprite.sheetPos.X;
				SpriteEffects se = SpriteEffects.None;
				if(direction == -1) {
					se = SpriteEffects.FlipHorizontally;
					spritePosX = width-(sprite.drawArea.Width + sprite.sheetPos.X);
				}

				game.spriteBatch.Draw(
					spriteSheet,
					new Vector2(
						position.X-room.cameraPos.X+spritePosX,
						position.Y-room.cameraPos.Y+sprite.sheetPos.Y
					),
					sprite.drawArea,
					Color.White,
					0.0f,
					new Vector2(0,0),
					1.0f,
					se,
					0.8f

				);
			}
		}
	}
}

