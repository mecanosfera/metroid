﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace metroid{
	
	public abstract class CharacterState : BaseState{

		public Character character;
		public string animationType = "idle";
		public int animationIndex = 0;
		public float animationTimer = 0.0f;
		protected bool flipAnimation = true;
		protected bool isAnimating = false;
		protected float walkSpeed;
		public bool debug = false;


		public CharacterState(Game1 g, string n, Character c) : base(g,n) {
			character = c;
			debug = game.debug;
		}

		public void startAnimation(string type){
			animationType = type;
			animationIndex = 0;
			animationTimer = 0.0f;
			isAnimating = false;

		}


		public Animation nextSprite(){
			Animation sprite;
			animationIndex++;
			if(animationIndex >= character.animations[animationType].Length) {
				animationIndex = 0;
			}
			sprite = character.animations[animationType][animationIndex];
			if(sprite.once) {
				if(!isAnimating) {
					isAnimating = true;
					return sprite;
				}
				return nextSprite();
			}
			return sprite;
		}
			

		protected virtual bool MoveX(float speed=0.0f){
			if(speed == 0.0f) {
				speed = walkSpeed;
			}
			character.position.X += speed * character.direction;
			if(character.DetectCollision()) {
				character.position.X -= speed*character.direction;
				float sub = 0.1f;
				while(speed>=0) {
					speed -= sub; 
					if(speed < 0) {
						sub = speed * -1;
					}
					character.position.X += sub * character.direction;
					if(character.DetectCollision()) {
						character.position.X -= sub * character.direction;
						return false;
					}
				}
			}
			return true;
		}

		protected virtual bool MoveY(float speed, int direction=1){
			character.position.Y += speed * direction;
			if(character.DetectCollision()) {
				character.position.Y -= speed*direction;
				float sub = 0.1f;
				while(speed>=0) {
					speed -= sub; 
					if(speed < 0) {
						sub = speed * -1;
					}
					character.position.Y += sub * direction;
					if(character.DetectCollision()) {
						character.position.Y -= sub * direction;
						return false;
					}
				}
			}
			return true;
		}

		public virtual bool MoveDown(float speed){
			return MoveY(speed);
		}

		public virtual bool MoveUp(float speed){
			return MoveY(speed,-1);
		}


		public override void Update(GameTime gameTime){
			foreach(Bullet b in character.bullets) {
				b.Update(gameTime);
			}
		}

		public override void Draw(GameTime gameTime){
			Animation sprite;
			float time = gameTime.ElapsedGameTime.Milliseconds/10.0f;
			//Console.WriteLine(time+","+animationTimer);
			if(debug) {
				sprite = new Animation(new Vector2(character.hitBox.X, character.hitBox.Y), new Rectangle(0, 440, character.hitBox.Width, character.hitBox.Height), 1f);
			} else {
                if(!character.room.world.paused){
                    animationTimer -= time;
                    if(animationTimer <= 0.0f) {
                        sprite = nextSprite(); 
                        animationTimer = sprite.time;
				    } else {
                        sprite = character.animations[animationType][animationIndex];
				    }
                } else {
                    sprite = character.animations[animationType][animationIndex];
                }
			}
			SpriteEffects se = SpriteEffects.None;
			float spritePosX = sprite.sheetPos.X;
			if(flipAnimation && character.direction == -1) {
				se = SpriteEffects.FlipHorizontally;
				spritePosX = character.width-(sprite.drawArea.Width + sprite.sheetPos.X);
			}
				
			game.spriteBatch.Draw(
				character.spriteSheet,
				new Vector2(
					character.position.X-character.room.cameraPos.X+spritePosX,
					character.position.Y-character.room.cameraPos.Y+sprite.sheetPos.Y
				),
				sprite.drawArea,
				Color.White,
				0.0f,
				new Vector2(0,0),
				1.0f,
				se,
				0.75f

			);

			if(character.bullets.Count > 0) {
				
				foreach(Bullet b in character.bullets) {
					if (b.active) {
						//Console.WriteLine (character.bullets.Count);
					}
					//b.Draw(gameTime);
				}
			}
		}

	}
}

