﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace metroid {
	
	public class Animation {


		public Vector2 sheetPos;
		public Rectangle drawArea;
		public float time;
		public bool once;

		public Animation(Vector2 sp, Rectangle r, float t, bool o=false) {
			sheetPos = sp;
			drawArea = r;
			time = t;
			once = o;
		}
	}
}

