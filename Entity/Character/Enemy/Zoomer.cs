﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace metroid{
	
	public class Zoomer : Enemy{

		public int vDirection = 1;

		public Zoomer(Room r, Vector2 p, int t=1) : base(r,"Zoomer",p,t){

			orientation = Orientation.Vertical;
			blockSize = 1;
			width = 32;
			height = 32;
			hitBox = new Rectangle(0,0,28,30);

			states.add("walk", new ZoomerWalkState(game,"walk",this));
			startState = "walk";



			if(type == 1) {
				animations = new Dictionary<string,Animation[]>() {
					{"idle", new Animation[] {
							new Animation(new Vector2(0, 0), new Rectangle(19, 15, 35, 31), 1f)
						}
					}, 
					{"walk_0", new Animation[] {
							new Animation(new Vector2(-1, 0), new Rectangle(19, 15, 35, 32), 4f),
							new Animation(new Vector2(-1, 0), new Rectangle(58, 15, 35, 32), 4f)
						}	
					},
					{"walk_90", new Animation[] {
							new Animation(new Vector2(0, -1), new Rectangle(244, 11, 32, 35), 4f),
							new Animation(new Vector2(0, -1), new Rectangle(278, 11, 32, 35), 4f)
						}	
					},
					{"walk_180", new Animation[] {
							new Animation(new Vector2(-1, 0), new Rectangle(167, 15, 35, 32), 4f),
							new Animation(new Vector2(-1, 0), new Rectangle(206, 15, 35, 32), 4f)
						}	
					},
					{"walk_270", new Animation[] {
							new Animation(new Vector2(0, -1), new Rectangle(96, 11, 32, 35), 4f),
							new Animation(new Vector2(0, -1), new Rectangle(132, 11, 32, 35), 4f)
						}	
					},
					{"damage_0", new Animation[] {
							new Animation(new Vector2(-1, 0), new Rectangle(315, 15, 35, 32), 4f),
							new Animation(new Vector2(-1, 0), new Rectangle(354, 15, 35, 32), 4f)
						}	
					},
					{"damage_90", new Animation[] {
							new Animation(new Vector2(0, -1), new Rectangle(541, 11, 32, 35), 4f),
							new Animation(new Vector2(0, -1), new Rectangle(576, 11, 32, 35), 4f)
						}	
					},
					{"damage_180", new Animation[] {
							new Animation(new Vector2(-1, 0), new Rectangle(463, 15, 35, 32), 4f),
							new Animation(new Vector2(-1, 0), new Rectangle(502, 15, 35, 32), 4f)
						}	
					},
					{"damage_270", new Animation[] {
							new Animation(new Vector2(0, -1), new Rectangle(393, 11, 32, 35), 4f),
							new Animation(new Vector2(0, -1), new Rectangle(428, 11, 32, 35), 4f)
						}	
					},
					{"explode", new Animation[] {
							new Animation(new Vector2(-16, -16), new Rectangle(1135, 466, 64, 64), 7f),
                            new Animation(new Vector2(0, 0), new Rectangle(1099, 482, 32, 32), 7f)
						}
					}
				};
			} else {
				animations = new Dictionary<string,Animation[]>() {
					{"idle", new Animation[] {
							new Animation(new Vector2(0, 0), new Rectangle(19, 15, 35, 31), 1f)
						}
					}, 
					{"walk_0", new Animation[] {
							new Animation(new Vector2(-1, 0), new Rectangle(611, 15, 35, 32), 4f),
							new Animation(new Vector2(-1, 0), new Rectangle(650, 15, 35, 32), 4f)
						}
					},
                    {"walk_90", new Animation[] {
                            new Animation(new Vector2(0, -1), new Rectangle(838, 11, 32, 35), 4f),
                            new Animation(new Vector2(0, -1), new Rectangle(872, 11, 32, 35), 4f)
                        }   
                    },
					{"walk_180", new Animation[] {
							new Animation(new Vector2(-1, 0), new Rectangle(759, 15, 35, 32), 4f),
							new Animation(new Vector2(-1, 0), new Rectangle(798, 15, 35, 32), 4f)
						}	
					},
                    {"walk_270", new Animation[] {
                            new Animation(new Vector2(0, -1), new Rectangle(690, 11, 32, 35), 4f),
                            new Animation(new Vector2(0, -1), new Rectangle(724, 11, 32, 35), 4f)
                        }   
                    },
					{"damage_0", new Animation[] {
							new Animation(new Vector2(-1, 0), new Rectangle(315, 15, 35, 32), 4f),
							new Animation(new Vector2(-1, 0), new Rectangle(354, 15, 35, 32), 4f)
						}	
					},
					{"damage_90", new Animation[] {
							new Animation(new Vector2(0, -1), new Rectangle(541, 11, 32, 35), 4f),
							new Animation(new Vector2(0, -1), new Rectangle(576, 11, 32, 35), 4f)
						}	
					},
					{"damage_180", new Animation[] {
							new Animation(new Vector2(-1, 0), new Rectangle(463, 15, 35, 32), 4f),
							new Animation(new Vector2(-1, 0), new Rectangle(502, 15, 35, 32), 4f)
						}	
					},
					{"damage_270", new Animation[] {
							new Animation(new Vector2(0, -1), new Rectangle(393, 11, 32, 35), 4f),
							new Animation(new Vector2(0, -1), new Rectangle(428, 11, 32, 35), 4f)
						}	
					},
					{"explode", new Animation[] {
							new Animation(new Vector2(0, 0), new Rectangle(1099, 482, 32, 32), 1000f),
							new Animation(new Vector2(-16, -16), new Rectangle(1135, 466, 64, 64), 1000f)
						}
					}
				};
			}

			states.Change(startState);

		}
	}
}

