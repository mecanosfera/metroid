﻿using System;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace metroid{
	
	public class Ripper : Enemy{
		
		public Ripper(Room r, Vector2 p, int t=1) : base(r,"Ripper",p,t){
			orientation = Orientation.Horizontal;
			blockSize = 1;
			width = 32;
			height = 32;
			hitBox = new Rectangle(0,7,32,18);
			direction = 1;

			states.add("walk", new RipperWalkState(game,"walk",this));
			startState = "walk";


			animations = new Dictionary<string,Animation[]>() { 
				{"walk", new Animation[] {
						new Animation(new Vector2(-2, 7), new Rectangle(918, 100, 35, 18), 12f)
					}
				},
				{"damage", new Animation[] {
						new Animation(new Vector2(-2, 7), new Rectangle(957, 100, 35, 18), 1f)
					}	
				},
				{"explode", new Animation[] {
						new Animation(new Vector2(0, 0), new Rectangle(1099, 482, 32, 32), 1f),
						new Animation(new Vector2(-16, -16), new Rectangle(1135, 466, 64, 64), 1f)
					}
				}
			};

            if(type==2){
                animations = new Dictionary<string,Animation[]>() { 
                    {"walk", new Animation[] {
                            new Animation(new Vector2(-2, 7), new Rectangle(997, 100, 35, 18), 12f)
                        }
                    },
                    {"damage", new Animation[] {
                            new Animation(new Vector2(-2, 7), new Rectangle(957, 100, 35, 18), 1f)
                        }   
                    },
                    {"explode", new Animation[] {
                            new Animation(new Vector2(0, 0), new Rectangle(1099, 482, 32, 32), 1f),
                            new Animation(new Vector2(-16, -16), new Rectangle(1135, 466, 64, 64), 1f)
                        }
                    }
                };
            }
			states.Change(startState);

		}


        public override bool Damage(){
            foreach(SamusBullet b in room.samusBullets) {                
                if(b.active && b.despawnTimer<=0.0f && TestCollision(b) && hp > 0) {
                    if(b.missile && type==1){
                        Die(true);
                        b.Despawn();
                        return true;
                    } else {
                        game.sfxDamageRipper.Play();
                        b.Despawn();
                    }
                    return false;
                }
            }
            return false;
        }
	}
}

