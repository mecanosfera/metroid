﻿using System;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace metroid{

    public class Zeb : Enemy{

        public bool playerDetected = false;
        public bool playerLocked = false;
        public float spawnTimer = 16f;


        public Zeb(Room r, Vector2 p, int t) : base(r,"Zeb",p,t){
            orientation = Orientation.Horizontal;
            blockSize = 1;
            width = 32;
            height = 32;
            hitBox = new Rectangle(0,7,32,18);
            direction = 1;
            baseHp = 2;
            hp = 2;

            states.add("walk", new ZebWalkState(game,"walk",this));
            startState = "walk";

            if(type == 1){
                animations = new Dictionary<string,Animation[]>() { 
                    {"walk", new Animation[] {
                            new Animation(new Vector2(-2, -2), new Rectangle(443, 131, 35, 35), 12f),
                            new Animation(new Vector2(-2, -2), new Rectangle(483, 131, 35, 35), 12f)
                        }
                    }
                };
            } else {
                animations = new Dictionary<string,Animation[]>() { 
                    {"walk", new Animation[] {
                            new Animation(new Vector2(-2, -2), new Rectangle(600, 131, 35, 35), 12f),
                            new Animation(new Vector2(-2, -2), new Rectangle(639, 131, 35, 35), 12f)
                        }
                    }
                };
            }

            animations.Add("damage", new Animation[] {
                new Animation(new Vector2(-2, -2), new Rectangle(522, 131, 35, 35), 12f),
                new Animation(new Vector2(-2, -2), new Rectangle(561, 131, 35, 35), 12f)
            });

            animations.Add("explode", new Animation[] {
                new Animation(new Vector2(0, 0), new Rectangle(1099, 482, 32, 32), 7f),
                new Animation(new Vector2(-16, -16), new Rectangle(1135, 466, 64, 64), 7f)
            });
                
        }


        public override bool Spawn(bool forceSpawn = false){
            if(room.OnCamera(this,true,false) && drop.taken){
                spawnTimer = 16f;
                playerDetected = false;
                playerLocked = false;
                dead = false;
                deadState = false;
                killed = false;
                position = spawnPosition;
                rotation = baseRotation;
                direction = 1;
                hp = baseHp;
                states.Change(startState);
                return true;
            }


            return false;
        }

    }
}
