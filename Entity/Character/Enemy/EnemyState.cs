﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace metroid{
	
	public class EnemyState: CharacterState{

		public Enemy self;
		public string damageType = "damage";

		public EnemyState (Game1 g, string n, Character c) : base(g,n,c){
			self = (Enemy) character;
		}

		public virtual string GetDamageType(){
			return damageType;
		}


		public override void Draw(GameTime gameTime){
			Animation sprite;
			float time = gameTime.ElapsedGameTime.Milliseconds/10.0f;
			if(debug) {
				sprite = new Animation(new Vector2(character.hitBox.X, character.hitBox.Y), new Rectangle(0, 440, character.hitBox.Width, character.hitBox.Height), 1f);
			} else {
				if(self.damagedTimer <= 0.0f) {
                    if(!self.room.world.paused){
                        animationTimer -= time;
                        if(animationTimer <= 0.0f) {
                            sprite = nextSprite(); 
                            animationTimer = sprite.time;
    					} else {
                            sprite = character.animations[animationType][animationIndex];
    					}
                    } else {
                        sprite = character.animations[animationType][animationIndex];
                    }
				} else {
                    sprite = character.animations[GetDamageType()][animationIndex];
                    if(!self.room.world.paused){
                        self.damagedTimer -= time;
                        if(self.damagedTimer <= 0.0f) {
                            self.damagedTimer = 0.0f;
					    }
                    }
				}
			}
			SpriteEffects se = SpriteEffects.None;
			float spritePosX = sprite.sheetPos.X;

			if(flipAnimation && character.direction == -1) {
				se = SpriteEffects.FlipHorizontally;
				spritePosX = character.width-(sprite.drawArea.Width + sprite.sheetPos.X);
			}
			game.spriteBatch.Draw(
				character.spriteSheet,
				new Vector2(
					character.position.X-character.room.cameraPos.X+spritePosX,
					character.position.Y-character.room.cameraPos.Y+sprite.sheetPos.Y
				),
				sprite.drawArea,
				Color.White,
				0.0f,
				new Vector2(0,0),
				1.0f,
				se,
				0.75f

			);
	
			/*if(character.bullets.Count > 0) {
				foreach(Bullet b in character.bullets) {
                    if(b.active){
                        b.Draw(gameTime);
                    }
				}
			}*/	
		}
	}
}

