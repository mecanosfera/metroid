﻿using System;
using Microsoft.Xna.Framework;

namespace metroid{
	
	public class SkreeAttackState : EnemyState{

		Skree me;

		public SkreeAttackState (Game1 g, string n, Character c) : base(g,n,c){
			me = (Skree)character;
			animationType="fall";
		}


		public override void Update(GameTime gameTime){
            if(!me.madeAttack){
                me.Damage();
            }
            float time = gameTime.ElapsedGameTime.Milliseconds / 10.0f;
            if(me.damagedTimer <= 0f){
                if(me.deathTimer > 0f){
                    me.deathTimer -= time;
                    if(me.deathTimer <= 0f){
                        me.deathTimer = 0f;
                        me.attack();
                    }
                } 
                if(me.postDeathTimer > 0f){
                    me.postDeathTimer -= time;
                    if(me.postDeathTimer <= 0f){
                        me.postDeathTimer = 0f;
                        me.dead = true;
                        me.deadState = true;
                    }
                }
            }
		}


        public override void Draw(GameTime gameTime){
            if(!me.madeAttack){
                base.Draw(gameTime);
            }
        }
            
	}
}

