﻿using System;
using Microsoft.Xna.Framework;

namespace metroid{
	
	public class SkreeIdleState : EnemyState{



		public SkreeIdleState(Game1 g, string n, Character c) : base(g,n,c){
			animationType="idle";
		}

		public override void Enter(string oldState){
			//((Skree)self).deathTimer = 16.0f;
		}


		public override void Update(GameTime gameTime){
            self.Damage();
            if(self.damagedTimer <= 0.0f) {
                float dif = character.room.player.position.X - character.position.X;
                if(dif <= 96f && dif >= -96f) {
                    character.states.Change("fall");		
				}
            }
		}
	}
}

