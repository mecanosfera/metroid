﻿using System;
using Microsoft.Xna.Framework;

namespace metroid{
	
	public class EnemyDieState : EnemyState{

		public float deathTime = 0.5f;

		public EnemyDieState (Game1 g, string n, Character c) : base(g,n,c){}

		public override void Enter(string lastState){
			startAnimation("explode");
			deathTime = 14f;
		}

		public override void Update(GameTime gameTime){
			if (deathTime <= 0f){
				deathTime = 20f;
				self.dead = true;
				if (self.killed){
                    Random r = new Random();
                    int drop = r.Next(0,12); //gerar num random
                    Console.WriteLine(drop);
					if(drop > 3) {
						if(drop <=8 && self.player.maxMissiles > 0) {
                            self.drop.Spawn(self.position, "missile");
						} else if (drop>8){
                            self.drop.Spawn(self.position, "energy");
						}
					}
				}
				self.states.Change(self.startState);
			} else {
				deathTime -= gameTime.ElapsedGameTime.Milliseconds/10.0f;
			}
		}

		public override void Draw(GameTime gameTime){
			if(self.killed){
				base.Draw(gameTime);
			}
		}
	}
}

