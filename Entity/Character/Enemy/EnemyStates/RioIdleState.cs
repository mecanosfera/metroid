﻿using System;
using Microsoft.Xna.Framework;

namespace metroid{
    
    public class RioIdleState : EnemyState{
        
        public RioIdleState(Game1 g, string n, Character c) : base(g,n,c){
            animationType="idle";
        }

        public override void Enter(string lastState){
            animationType="idle";
            Console.WriteLine("idle");
        }
    

        public override void Update(GameTime gameTime){
            self.Damage();
            if(self.damagedTimer <= 0.0f) {
                float dif = character.room.player.position.X - character.position.X;
                if(dif <= 128f && dif >= -128f) {
                    character.states.Change("attack");        
                }
            }
        }
	}
}

