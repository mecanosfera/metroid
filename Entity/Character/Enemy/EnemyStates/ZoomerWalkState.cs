﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace metroid{
	
    public class ZoomerWalkState : EnemyState{
		
		public ZoomerWalkState(Game1 g, string n, Character c) : base(g,n,c){
			animationType = "walk_" + character.rotation;
			flipAnimation = false;
			walkSpeed = 2f;
			//character.direction = -1;
		}

		public override string GetDamageType(){
			return "damage_" + character.rotation;
		}


		public override void Update(GameTime gameTime){
			self.Damage();
			if(self.damagedTimer<=0.0f) {
				animationType = "walk_" + character.rotation;
				Vector2 dir = character.position;

				float speed = walkSpeed;

                if(character.rotation == 0) {
                    character.orientation = Entity.Orientation.Horizontal;
                    character.hitBox.X = 2;
                    character.hitBox.Y = 4;
                    character.position.X -= speed * character.direction;
                    if(character.DetectCollision()) {
                        character.position.X += speed * character.direction;
                        character.rotation -= 90 * character.direction;
                        if(character.rotation < 0) {
                            character.rotation = 270;
                        }
                    }
                    character.orientation = Entity.Orientation.Vertical;
                    character.position.Y += speed;
                    if(character.DetectCollision()) {
                        character.position.Y -= speed;
                    } else {
                        character.rotation += 90 * character.direction;
                        if(character.rotation < 0) {
                            character.rotation = 270;
                        }
                    }
                } else if(character.rotation == 90) {
                    character.orientation = Entity.Orientation.Vertical;
                    character.hitBox.X = 4;
                    character.hitBox.Y = 2;
                    character.position.Y += speed * character.direction;
                    if(character.DetectCollision()) {
                        character.position.Y -= speed * character.direction;
                        character.rotation -= 90 * character.direction;
                    }
                    character.orientation = Entity.Orientation.Horizontal;
                    character.position.X += speed;
                    if(character.DetectCollision()) {
                        character.position.X -= speed;
                    } else {
                        character.rotation += 90 * character.direction;
                    }
                }
                if(character.rotation == 180) {
                    character.orientation = Entity.Orientation.Horizontal;
                    character.hitBox.X = 2;
                    character.hitBox.Y = 0;
                    character.position.X += speed * character.direction;
                    if(character.DetectCollision()) {
                        character.position.X -= speed * character.direction;
                        character.rotation -= 90 * character.direction;
                    }
                    character.orientation = Entity.Orientation.Vertical;
                    character.position.Y -= speed;
                    if(character.DetectCollision()) {
                        character.position.Y += speed;
                    } else {
                        character.rotation += 90 * character.direction;
                    }
                } else if(character.rotation == 270) {
                    character.orientation = Entity.Orientation.Vertical;
                    character.hitBox.X = 0;
                    character.hitBox.Y = 2;
                    character.position.Y -= speed * character.direction;
                    if(character.DetectCollision()) {
                        character.position.Y += speed * character.direction;
                        character.rotation -= 90 * character.direction;
                        if(character.rotation == 360) {
                            character.rotation = 0;
                        }
                    }
                    character.orientation = Entity.Orientation.Horizontal;
                    character.position.X -= speed;
                    if(character.DetectCollision()) {
                        character.position.X += speed;
                    } else {
                        character.rotation += 90 * character.direction;
                        if(character.rotation == 360) {
                            character.rotation = 0;
                        }
                    }
                }
			}
	
		}

	}
}

