﻿using System;
using Microsoft.Xna.Framework;

namespace metroid{
	
	public class SkreeFallState : EnemyState {

		Samus player;
		float speed;
        Skree self;

		public SkreeFallState(Game1 g, string n, Character c) : base(g,n,c){
			animationType="fall";
			flipAnimation = false;
			player = character.room.player;
			speed = 1.70f;
            self = (Skree)character;
		}


		public override void Update(GameTime gameTime){
            self.Damage();
            if(self.damagedTimer <= 0.0f) {
                if(MoveDown(speed * 3.6f)) {
                    if(player.position.X > character.position.X) {
                        MoveX(speed);
    				} else if(player.position.X < character.position.X) {
                        MoveX(speed * -1);
    				}
    			} else {
                    character.states.Change("attack");
                }
            }
		}


	}
}

