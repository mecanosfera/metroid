﻿using System;
using Microsoft.Xna.Framework;

namespace metroid{
    
    public class ZebWalkState : EnemyState{



        public Samus player;
        public Zeb me;

        public ZebWalkState(Game1 g, string n, Character c) : base(g,n,c){
            animationType = "walk";
            //flipAnimation = true;
            walkSpeed = 2.5f;
            player = c.room.player;
            me = (Zeb)self;
		}


        public override void Update(GameTime gameTime){
            self.Damage();
            if(((Zeb)self).spawnTimer > 0f){
                ((Zeb)self).spawnTimer -= gameTime.ElapsedGameTime.Milliseconds / 10f;
                if(((Zeb)self).spawnTimer <= 0f){
                    ((Zeb)self).spawnTimer = 0f;
                }
            } else {
                if(!me.playerDetected){
                    if(player.position.X < me.position.X){
                        me.direction = -1;
                    }
                    if(player.position.X >= me.position.X){
                        me.direction = 1;
                    }
                    if(((float)Math.Abs(player.position.X - me.position.X)) <= 192){
                        me.playerDetected = true;
                    }
                } else {
                    if(!me.playerLocked){
                        me.position.Y -= 4f;
                        if(me.position.Y <= player.position.Y){
                            me.playerLocked = true;
                        }
                    } else {
                        me.position.X += 4f * me.direction;
                    }
                }
            }
        }
	}
}

