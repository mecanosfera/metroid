﻿using System;
using Microsoft.Xna.Framework;

namespace metroid{

	public class RipperWalkState : EnemyState{

		public RipperWalkState(Game1 g, string n, Character c) : base(g,n,c){
			animationType = "walk";
			flipAnimation = true;
			walkSpeed = 1.5f;
		}

		public override void Update(GameTime gameTime){
            self.Damage();
			if(!MoveX()) {
				self.direction *= -1;
			}
		}

	}
}

