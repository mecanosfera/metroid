﻿using System;
using Microsoft.Xna.Framework;

namespace metroid{
    
    public class RioAttackState : EnemyState{
        
        public float walkTime = 0f;
        public float vSpeed = 0f;
        public string mode="fall";

        public RioAttackState(Game1 g, string n, Character c) : base(g,n,c){
            
        }

        public override void Enter(string lastState){
            animationType = "walk";
            walkSpeed = 2f;
            mode = "fall";
        }

        public override void Update(GameTime gameTime){
            self.Damage();
            if(mode == "fall"){
                vSpeed = (float)Math.Sin(walkTime) * 4.5f;
                walkTime += (float)gameTime.ElapsedGameTime.TotalSeconds * 2.0f;
                character.position.Y += vSpeed;
                if(character.DetectCollision()) {
                    character.position.Y -= vSpeed;
                    if(self.lastBlockDetected.position.Y < self.position.Y){
                        self.states.Change("idle");
                    } else {
                        mode = "end";
                    }
                }
                if(self.position.Y + 32 >= self.player.position.Y){
                    mode = "follow";
                }
            } else if(mode == "follow"){
                string pstate = self.player.states.activeState.name;
                if(pstate == "walk" || pstate == "idle" || pstate == "ball"){
                    character.position.Y += 4f;
                    if(character.DetectCollision()){
                        character.position.Y -= 4f;
                    }
                    //MoveDown(4f);
                } else {
                    mode = "end";
                }
            } else if(mode == "end"){
                vSpeed = (float)Math.Sin(walkTime) * 5.5f;
                walkTime += (float)gameTime.ElapsedGameTime.TotalSeconds * 2.0f;
                character.position.Y += vSpeed;
                if(character.DetectCollision()) {
                    character.position.Y -= vSpeed;
                    if(self.lastBlockDetected.position.Y<self.position.Y){
                        self.states.Change("idle");
                    }
                }
            }
            Console.WriteLine(mode);
            if(!MoveX(2f)) {
                self.direction *= -1;
            }
        }

	}
	
}

