﻿using System;
using Microsoft.Xna.Framework;

namespace metroid{
    
    public class WaverWalkState : EnemyState{

        public float walkTime = 0f;
        public float vSpeed = 0f;


        public WaverWalkState(Game1 g, string n, Character c) : base(g,n,c){
            animationType = "walk";
            //flipAnimation = true;
            walkSpeed = 2f;
            //self.direction = -1;
		}

        public override void Update(GameTime gameTime){
            Vector2 dir = character.position;
            self.Damage();
            vSpeed = (float)Math.Sin(walkTime) * 4.5f;
            walkTime += (float)gameTime.ElapsedGameTime.TotalSeconds * 2.0f;
            character.position.Y += vSpeed;
            if(character.DetectCollision()) {
                character.position.Y = dir.Y;
            }
            if(!MoveX()) {
                self.direction *= -1;
            }
        }
	}
}

