﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace metroid{
	
	public class Enemy : Character {

		public bool dead = false;
		public bool deadState = false;
		public bool killed = false;
		public bool loaded = true;
		public Vector2 spawnPosition;
		public string startState = "idle";
		public float damagedTimer = 0.0f;
		public Samus player;
        public SoundEffect damageSound;
        public Random rand;
        public Item drop;
        public int type = 1;

		
        public Enemy(Room r, string n, Vector2 p, int t=1) : base(r,n) {
            type = t;
            baseHp = 2;
            if(type == 2){
                baseHp = 4;
            }
            hp = baseHp;
			spriteSheet = game.enemySprites;
			states = new StateMachine(new Dictionary<string, BaseState>(){
				{"die", new EnemyDieState(r.game,"die",this)}
			},"die");
			position = p;
			spawnPosition = p;
			player = r.player;
            damageSound = game.sfxDamageEnemy;
            rand = new Random();
            drop = new Item(room,"energy","drop",position);
            drop.taken = true;
            damage = 8;
		}

		public static Enemy EnemySpawner(Room room, Vector2 position, string enemyName, int rotation, int d, int enemyType=1){
			Enemy enemy = null;
            if(enemyName.ToLower() == "zoomer") {
                enemy = new Zoomer(room, position, enemyType);
                enemy.rotation = rotation;
                enemy.baseRotation = rotation;
			} else if(enemyName.ToLower() == "skree") {
                enemy = new Skree(room, position, enemyType);
			} else if(enemyName.ToLower() == "rio") {
                enemy = new Rio(room, position, enemyType);
			} else if(enemyName.ToLower() == "ripper"){
                enemy = new Ripper(room, position, enemyType);
            } else if(enemyName.ToLower() == "waver"){
                enemy = new Waver(room, position, enemyType);
            } else if(enemyName.ToLower() == "zeb"){
                enemy = new Zeb(room, position, enemyType);
            } else {
                enemy = new Skree(room, position, enemyType);
            }
			enemy.direction = d;
			enemy.baseDirection = d;
			enemy.dead = false;
			enemy.deadState = false;
            enemy.hp = enemy.baseHp;
			return enemy;
		}

		public virtual void Die(bool k = false){
			damagedTimer = 0.0f;
			deadState = true;
			killed = k;
			if(killed) {
				states.Change("die");
			}
		}

        public virtual void ItemDespawn(){}

		public override bool Damage(){
			foreach(SamusBullet b in room.samusBullets) {                
				if(b.active && b.despawnTimer<=0.0f && TestCollision(b) && hp > 0) {
                    ((EnemyState)states.activeState).animationIndex = 0;
                    game.sfxDamageEnemy.Play();
					b.Despawn();
					hp -= b.damage;
					if(hp > 0) {
						damagedTimer = 10.0f;
					} else {
						Die(true);
					}
					return true;
				}
			}
			return false;
		}

        public virtual bool Spawn(bool forceSpawn = false){
            if(forceSpawn || (!room.OnCamera(this, false) && !room.OnCamera(this, true))){
                dead = false;
                deadState = false;
                killed = false;
                position = spawnPosition;
                rotation = baseRotation;
                direction = baseDirection;
                hp = baseHp;
                states.Change(startState);
                return true;
            }
            return false;
			//}
			//return false;
		}

        /*public override bool TestCollision(Entity e){
            if(base.TestCollision(e)){
                if(e.GetType() == typeof(Block)){
                    if(((Block)e).lava > -1){
                        return false;
                    }
                    return true;
                }
            }
            return false;
        }*/


		public override void Update(GameTime gameTime){
			states.activeState.Update(gameTime);

		}

		public override void Draw(GameTime gameTime){
			base.Draw(gameTime);
			if(game.debug) {
				game.spriteBatch.Draw(
					spriteSheet,
					new Vector2(
						position.X-room.cameraPos.X,
						position.Y-room.cameraPos.Y-160
					),
					new Rectangle(0,728,32,224),
					Color.White,
					0.0f,
					new Vector2(0,0),
					1.0f,
					SpriteEffects.None,
					1.0f

				);

			}
			if(game.borderDebug) {
				game.spriteBatch.Draw(
					spriteSheet,
					new Vector2(
						position.X-room.cameraPos.X,
						position.Y-room.cameraPos.Y
					),
					new Rectangle(1156,357,width,height),
					Color.White,
					0.0f,
					new Vector2(0,0),
					1.0f,
					SpriteEffects.None,
					0.9f

				);


			}
		}
	}
}

