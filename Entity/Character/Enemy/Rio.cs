﻿using System;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace metroid{
	
	public class Rio: Enemy{
		
		public Rio(Room r, Vector2 p, int t=1) : base(r,"Rio",p,t){
			
            orientation = Orientation.Vertical;
            blockSize = 1;
            width = 32;
            height = 32;
            hitBox = new Rectangle(0,10,32,22);
            baseHp = 3;
            hp = 3;
            direction = 1;

            states.add("idle", new RioIdleState(game,"idle",this));
            states.add("attack", new RioAttackState(game,"attack",this));
            startState = "idle";


            animations = new Dictionary<string,Animation[]>() { 
                {"idle", new Animation[] {
                        new Animation(new Vector2(-10, -4), new Rectangle(465, 76, 52, 42), 12f),
                        new Animation(new Vector2(-8, -2), new Rectangle(522, 76, 52, 42), 12f)
                    }
                },
                {"walk", new Animation[] {
                        new Animation(new Vector2(-10, -4), new Rectangle(465, 76, 52, 42), 6f),
                        new Animation(new Vector2(-8, -2), new Rectangle(522, 76, 52, 42), 6f)
                    }
                }
            };
            

            animations.Add("damage", new Animation[] {
                new Animation(new Vector2(-10, -7), new Rectangle(578, 76, 52, 42), 12f),
                new Animation(new Vector2(-2, -2), new Rectangle(635, 76, 52, 42), 12f)
            });

            animations.Add("explode", new Animation[] {
                new Animation(new Vector2(0, 0), new Rectangle(1099, 482, 32, 32), 7f),
                new Animation(new Vector2(-16, -16), new Rectangle(1135, 466, 64, 64), 7f)
            });
		}
	}
}

