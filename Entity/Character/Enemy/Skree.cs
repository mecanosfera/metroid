﻿using System;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace metroid{
	
	public class Skree : Enemy{


		public float baseDeathTimer = 60f;
		public float deathTimer = 60f;
		public float baseDespawnTimer = 0f;
		public float despawnTimer = 0f;
        public float postDeathTimer = 0f;
        public bool madeAttack = false;

		public Skree(Room r, Vector2 p, int t) : base(r,"Skree",p,t){

			orientation = Orientation.Vertical;
			blockSize = 2;
			width = 32;
			height = 64;
			hitBox = new Rectangle(7,0,17,53);
			direction = 1;

			bullets = new List<Bullet>(){ 
				new Bullet(this,"skree_left"),
				new Bullet(this,"skree_leftup"),
				new Bullet(this,"skree_right"),
				new Bullet(this,"skree_rightup")
			};

			Rectangle[] spritePos = new Rectangle[]{ new Rectangle(446,102,16,16) };
			Rectangle bulletHitBox = new Rectangle(8,8,16,16);
			float maxDistance = 14f;
			float speedY = 2.4f*3;
			float speedX = 0.8f*3;

			bullets[0].direction = -1;
			bullets[0].vDirection = 0;
			bullets[0].maxDistance = maxDistance;
			bullets[0].spritePos = spritePos;
			bullets[0].hitBox = bulletHitBox;
			bullets[0].speedX = speedY;
			bullets[1].direction = -1;
			bullets[1].vDirection = -1;
			bullets[1].speedY = speedY;
			bullets[1].speedX = speedX;
			bullets[1].maxDistance = maxDistance;
			bullets[1].spritePos = spritePos;
			bullets[1].hitBox = bulletHitBox;
			bullets[2].direction = 1;
			bullets[2].vDirection = 0;
			bullets[2].maxDistance = maxDistance;
			bullets[2].spritePos = spritePos;
			bullets[2].hitBox = bulletHitBox;
			bullets[2].speedX = speedY;
			bullets[3].direction = 1;
			bullets[3].vDirection = -1;
			bullets[3].maxDistance = maxDistance;
			bullets[3].spritePos = spritePos;
			bullets[3].hitBox = bulletHitBox;
			bullets[3].speedY = speedY;
			bullets[3].speedX = speedX;
		
			states.add("idle", new SkreeIdleState(game,"idle",this));
			states.add("fall", new SkreeFallState(game,"fall",this));
			states.add("attack", new SkreeAttackState(game,"attack",this));
			startState = "idle";

			states.Change(startState);
			animations = new Dictionary<string,Animation[]>() { 
				{"idle", new Animation[] {
						new Animation(new Vector2(-2, 0), new Rectangle(32, 65, 35, 53), 12f),
						new Animation(new Vector2(7, 0), new Rectangle(71, 65, 17, 53), 12f),
						new Animation(new Vector2(7, 0), new Rectangle(93, 65, 17, 53), 12f)
					}
				}, 
				{"fall", new Animation[] {
						new Animation(new Vector2(-2, 0), new Rectangle(32, 65, 35, 53), 3f),
						new Animation(new Vector2(7, 0), new Rectangle(71, 65, 17, 53), 3f),
						new Animation(new Vector2(7, 0), new Rectangle(93, 65, 17, 53), 3f)
					}	
				},
				{"attack", new Animation[] {
						new Animation(new Vector2(-2, 0), new Rectangle(32, 65, 35, 53), 3f),
						new Animation(new Vector2(7, 0), new Rectangle(71, 65, 17, 53), 3f),
						new Animation(new Vector2(7, 0), new Rectangle(93, 65, 17, 53), 3f)
					}	
				},
				{"damage", new Animation[] {
                        new Animation(new Vector2(-2, 0), new Rectangle(114, 65, 35, 53), 3f),
                        new Animation(new Vector2(7, 0), new Rectangle(154, 65, 17, 53), 3f),
                        new Animation(new Vector2(7, 0), new Rectangle(175, 65, 17, 53), 3f)
					}	
				},
				{"explode", new Animation[] {
						new Animation(new Vector2(0, 0), new Rectangle(1099, 482, 32, 32), 7f),
						new Animation(new Vector2(-16, -16), new Rectangle(1135, 466, 64, 64), 7f)
					}
				}
			};



		}

        public override bool Spawn(bool forceSpawn = false){
            if(base.Spawn(forceSpawn)){
                deathTimer = baseDeathTimer;
                despawnTimer = 0.0f;
                madeAttack = false;
                postDeathTimer = 0f;
                return true;
            }
			return false;
		}

		

        public void attack(){
            //Console.WriteLine(":::attack");
            bullets[0].position.X = position.X;
            bullets[0].position.Y = position.Y + 16;
            bullets[0].active = true;
            bullets[1].position.X = position.X;
            bullets[1].position.Y = position.Y + 16;
            bullets[1].active = true;
            bullets[2].position.X = position.X;
            bullets[2].position.Y = position.Y + 16;
            bullets[2].active = true;
            bullets[3].position.X = position.X;
            bullets[3].position.Y = position.Y + 16;
            bullets[3].active = true;
            postDeathTimer = 30f;
            madeAttack = true;
        }

		public override void Die(bool k = false){
            deathTimer = 0.0f;
			base.Die(true);
		}
	}
}

