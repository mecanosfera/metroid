﻿using System;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace metroid{
    
    public class Waver : Enemy{
        
        public Waver(Room r, Vector2 p, int t) : base(r,"Waver",p,t){

            orientation = Orientation.Vertical;
            blockSize = 1;
            width = 32;
            height = 32;
            hitBox = new Rectangle(0,7,32,18);
            direction = 1;

            states.add("walk", new WaverWalkState(game,"walk",this));
            startState = "walk";

            if(type == 1){
                animations = new Dictionary<string,Animation[]>() { 
                    {"walk", new Animation[] {
                            new Animation(new Vector2(-2, -2), new Rectangle(169, 131, 35, 35), 96f),
                            new Animation(new Vector2(-2, -2), new Rectangle(130, 131, 35, 35), 12f),
                            new Animation(new Vector2(-2, -2), new Rectangle(90, 131, 35, 35), 12f),
                            new Animation(new Vector2(-2, -2), new Rectangle(130, 131, 35, 35), 12f)
                            ,

                        }
                    }
                };
            } else {
                animations = new Dictionary<string,Animation[]>() { 
                    {"walk", new Animation[] {
                            new Animation(new Vector2(-2, -2), new Rectangle(286, 131, 35, 35), 96f),
                            new Animation(new Vector2(-2, -2), new Rectangle(247, 131, 35, 35), 12f),
                            new Animation(new Vector2(-2, -2), new Rectangle(208, 131, 35, 35), 12f),
                            new Animation(new Vector2(-2, -2), new Rectangle(247, 131, 35, 35), 12f)

                        }
                    }
                };
            }

            animations.Add("damage", new Animation[] {
                new Animation(new Vector2(-2, -2), new Rectangle(326, 131, 35, 35), 12f),
                new Animation(new Vector2(-2, -2), new Rectangle(365, 131, 35, 35), 12f),
                new Animation(new Vector2(-2, -2), new Rectangle(404, 131, 35, 35), 12f)
            });

            animations.Add("explode", new Animation[] {
                new Animation(new Vector2(0, 0), new Rectangle(1099, 482, 32, 32), 7f),
                new Animation(new Vector2(-16, -16), new Rectangle(1135, 466, 64, 64), 7f)
            });


		}
	}
}

