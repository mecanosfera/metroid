﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace metroid {
	
	public class Block : Entity {

		public bool breakable = false;
        public bool broken = false;
        public float brokenTime = 0f;
        public Rectangle spritePos;
		public int[] pos;
		public int gid;
        public int lava = -1;
        public int x;
        public int y;


		public Block(Room r, int g, int[] p): base(r){
			gid = g;
			blockSize = 1;
			hitBox = new Rectangle(0,0,32,32);
			pos = p;
			width = 32;
			height = 32;

			x = ((gid-1) % 16)*32;
	        y = ((int) (gid-1)/16)*32;
			spritePos = new Rectangle(x, y, 32, 32);
			position = new Vector2((p[0])*32,(p[1])*32);
		}

        public void destroy(){
            if(!broken && breakable){
                broken = true;
                collision = false;
                spritePos = new Rectangle(160,192,32,32);
                brokenTime = 0f;
            }
        }

        public override void Update(GameTime gameTime){
            if(broken){
                brokenTime += gameTime.ElapsedGameTime.Milliseconds / 10f;
                if(brokenTime > 8f){
                    if(brokenTime < 360f){
                        spritePos = new Rectangle(0, 0, 0, 0);
                    } else if(brokenTime >=360f && brokenTime < 370f){
                        spritePos = new Rectangle(160,192,32,32);
                    } else {
                        broken = false;
                        collision = true;
                        spritePos = new Rectangle(x, y, 32, 32);
                    }
                }
            }
            base.Update(gameTime);
        }
			
	}
}

