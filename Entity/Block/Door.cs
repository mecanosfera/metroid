﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace metroid {

	public class Door {

		public string color = "blue";
		public bool open = false;
		public Room room;
		public Room nextRoom;
		public List<DoorBlock> frameBlocks;
		public List<DoorBlock> doorBlocks;
		public int x;
		public int y;
		public int direction = 1;
        public float openTimer = 0f;
        public float closeTimer = 0f;
        public float stayOpenTimer = 0f;
        public float beforeOpen = 0f;
        public bool normal = true;
        public int missileHits = 5;

		public Door(Room r, string next_room, int dir){
			room = r;
			frameBlocks = new List<DoorBlock>();
			doorBlocks = new List<DoorBlock>();
			nextRoom = (Room) room.world.rooms.get(next_room);
			direction = dir;
		}

        public void Open(){
            if(!normal){
                normal = true;
                foreach(DoorBlock d in doorBlocks){
                    d.gid -= 2;
                    d.x = ((d.gid-1) % 16)*32;
                    d.y = ((int) (d.gid-1)/16)*32;
                    d.spritePos = new Rectangle(x, y, 32, 32);

                    /*Console.WriteLine(d.x);
                    d.x -= 2;
                    Console.WriteLine("->"+d.x);
                    Console.WriteLine("");
                    d.spritePos = new Rectangle(x, y, 32, 32);*/
                }
            }
            open = true;
            if(room.world.transition){
                beforeOpen = 240f;
                openTimer = 20f;
                closeTimer = 20f;
            } else {
                beforeOpen = 0f;
                openTimer = 10f;
                closeTimer = 20f;
            }
            stayOpenTimer = 160f;
            if(beforeOpen <= 0f){
                doorBlocks[0].spritePos = new Rectangle(doorBlocks[0].x + (4 * 32), doorBlocks[0].y, 32, 32);
                doorBlocks[1].spritePos = new Rectangle(doorBlocks[1].x + (4 * 32), doorBlocks[1].y, 32, 32);
                doorBlocks[2].spritePos = new Rectangle(doorBlocks[2].x + (4 * 32), doorBlocks[2].y, 32, 32);
                room.game.sfxDoor.Play();
            }
        }

        public void StayOpen(){
            openTimer = 0;
            doorBlocks[0].open = true;
            doorBlocks[0].collision = false;
            doorBlocks[1].open = true;
            doorBlocks[1].collision = false;
            doorBlocks[2].open = true;
            doorBlocks[2].collision = false;
            doorBlocks[0].spritePos = new Rectangle(doorBlocks[0].x, doorBlocks[0].y+(4*32), 32, 32);
            doorBlocks[1].spritePos = new Rectangle(doorBlocks[1].x, doorBlocks[1].y+(4*32), 32, 32);
            doorBlocks[2].spritePos = new Rectangle(doorBlocks[2].x, doorBlocks[2].y+(4*32), 32, 32);
        }

        public void Close(){
            stayOpenTimer = 0f;
            doorBlocks[0].spritePos = new Rectangle(doorBlocks[0].x+(4*32), doorBlocks[0].y, 32, 32);
            doorBlocks[1].spritePos = new Rectangle(doorBlocks[1].x+(4*32), doorBlocks[1].y, 32, 32);
            doorBlocks[2].spritePos = new Rectangle(doorBlocks[2].x+(4*32), doorBlocks[2].y, 32, 32);
            doorBlocks[0].open = false;
            doorBlocks[0].collision = true;
            doorBlocks[1].open = false;
            doorBlocks[1].collision = true;
            doorBlocks[2].open = false;
            doorBlocks[2].collision = true;
            if(!room.world.transition){
                room.game.sfxDoor.Play();
            }
        }

        public virtual void Update(GameTime gameTime){
            if(open){
                if(beforeOpen > 0f){
                    beforeOpen -= (float)gameTime.ElapsedGameTime.Milliseconds / 10f;
                    if(beforeOpen <= 0f){
                        doorBlocks[0].spritePos = new Rectangle(doorBlocks[0].x + (4 * 32), doorBlocks[0].y, 32, 32);
                        doorBlocks[1].spritePos = new Rectangle(doorBlocks[1].x + (4 * 32), doorBlocks[1].y, 32, 32);
                        doorBlocks[2].spritePos = new Rectangle(doorBlocks[2].x + (4 * 32), doorBlocks[2].y, 32, 32);
                        room.game.sfxDoor.Play();
                    }
                } else {
                    if(openTimer > 0f){
                        openTimer -= (float)gameTime.ElapsedGameTime.Milliseconds / 10f;
                        if(openTimer <= 0f){
                            if(!room.world.transition){
                                StayOpen();
                            } else {
                                Close();
                            }
                        }
                    } else if(stayOpenTimer > 0f){
                        stayOpenTimer -= (float)gameTime.ElapsedGameTime.Milliseconds / 10f;
                        if(stayOpenTimer <= 0f || room.world.transition){
                            Close();
                        }
                    } else if(closeTimer > 0f){
                        closeTimer -= (float)gameTime.ElapsedGameTime.Milliseconds / 10f;
                        if(closeTimer <= 0f){
                            closeTimer = 0f;
                            doorBlocks[0].spritePos = new Rectangle(doorBlocks[0].x, doorBlocks[0].y, 32, 32);
                            doorBlocks[1].spritePos = new Rectangle(doorBlocks[1].x, doorBlocks[1].y, 32, 32);
                            doorBlocks[2].spritePos = new Rectangle(doorBlocks[2].x, doorBlocks[2].y, 32, 32);
                            open = false;
                        }
                    }
                }
            }
        }


		public void Add(DoorBlock block){
			block.direction = direction;
			if(block.isDoorFrame) {
				frameBlocks.Add(block);
			} else {
				doorBlocks.Add(block);
			}
		}
	}
}

