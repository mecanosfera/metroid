﻿using System;
using Microsoft.Xna.Framework;

namespace metroid{
	
	public class DoorBlock : Block{

		public bool isDoorFrame = false;
		public bool open = false; 
		public Door door;

		public DoorBlock(Door dr, int gid, int[] p, bool frame, int d=1): base(dr.room,gid,p){
			isDoorFrame = frame;
			direction = d;
			if(!isDoorFrame) {
				if(direction == 1) {
					hitBox = new Rectangle(0, 0, 16, 32);
				} else {
					hitBox = new Rectangle(16, 0, 16, 32);
				}
			}
			door = dr;
		}

        public void Open(){
            if(!open){
                door.Open();
            }
        }




        public override void Draw(GameTime gameTime){
            if(isDoorFrame || !open){
                base.Draw(gameTime);
            } 
        }
	}
}

