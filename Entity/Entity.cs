﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Audio;

namespace metroid {
	
	public abstract class Entity {

		public Game1 game;
		public StateMachine states;
		public Room room;
		public string name;
		public int[] blockPosition;
		public int blockSize;
		public int width;
		public int height;
		public Vector2 position;
		public Rectangle hitBox;
		public bool collision = true;
		public enum Orientation {Horizontal,Vertical};
		public Orientation orientation;
		public Dictionary<string,Animation[]> animations;
		public Animation[] animation;
		public float AnimationTime;
		public Texture2D spriteSheet;
		public int direction = 0;
		public Block lastBlockDetected;
        public SoundEffectInstance soundInstance;
        public int damage = 0;



		public Entity(Room r, string n = null){
			game = r.game;
			room = r;
			name = n;
		}

		public virtual void Update(GameTime gameTime){}

		public virtual void Draw(GameTime gameTime){}

        public virtual void Sound(){}

		public int[][] OverlapingBlocks(){
			int[][] blocks;
			int rx = (int) (position.X % 32);
			int ry = (int) position.Y % 32 ;
			int x = (int)(position.X / 32);
			int y = (int) (position.Y / 32);
			//Console.WriteLine("x:"+x+",y"+y);
			int numBlocks = blockSize;
			if(rx > 0 && ry > 0) {
				numBlocks = (blockSize * 2) + 2;
			} else {
				if(orientation == Orientation.Vertical) {
					if(rx > 0) {
						numBlocks = blockSize * 2;
					} else if(ry > 0) {
						numBlocks = blockSize + 1;
					}
				} else {
					if(ry > 0) {
						numBlocks = blockSize * 2;
					} else if(rx > 0) {
						numBlocks = blockSize + 1;
					}
				}
			}

			blocks = new int[numBlocks][];
			int c = 0;

			if(blockSize == 0) {
				blocks[0] = new int[2]{ x, y };
			} else {
				if(orientation == Orientation.Vertical) {
					if(numBlocks <= blockSize + 1) {
						for(int i = 0; i < numBlocks; i++) {
							blocks[c] = new int[2]{ x, y + i };
							c++;
						}
					} else if(numBlocks == blockSize * 2) {
						for(int i = 0; i < blockSize; i++) {
							blocks[c] = new int[2]{ x, y + i };
							c++;
							blocks[c] = new int[2]{ x+1, y + i };
							c++;
						}
					} else if(numBlocks == (blockSize * 2) + 2) {
						for(int i = 0; i < blockSize+1; i++) {
							blocks[c] = new int[2]{ x, y + i };
							c++;
							blocks[c] = new int[2]{ x+1, y + i };
							c++;
						}
					}
				} else if(orientation == Orientation.Horizontal) {
					if(numBlocks <= blockSize + 1) {
						for(int i = 0; i < numBlocks; i++) {
							blocks[c] = new int[2]{ x+i, y};
							c++;
						}
					} else if(numBlocks == blockSize * 2) {
						for(int i = 0; i < blockSize; i++) {
							blocks[c] = new int[2]{ x+i, y };
							c++;
							blocks[c] = new int[2]{ x+i, y + 1 };
							c++;
						}
					} else if(numBlocks == (blockSize * 2) + 2) {
						for(int i = 0; i < blockSize+1; i++) {
							blocks[c] = new int[2]{ x+i, y };
							c++;
							blocks[c] = new int[2]{ x+i, y+1 };
							c++;
						}
					}
				} 
			}

			return blocks;

		}



		public virtual bool TestCollision(Entity e){
			Vector2[] myMinMax = GetMinMax();
			Vector2[] testMinMax = e.GetMinMax();
			Vector2 myMin = myMinMax[0];
			Vector2 myMax = myMinMax[1];
			Vector2 testMin = testMinMax[0];
			Vector2 testMax = testMinMax[1];

			if((testMax.X >= myMin.X) && (testMax.Y >= myMin.Y) &&
			    (testMin.X <= myMax.X) && (testMin.Y <= myMax.Y)) {
				return true;
			}
			return false;
		}

		public Vector2[] GetMinMax(bool hitbox=true){
			Vector2 min = position;
			if(hitbox) {
				min.X += hitBox.X;
				min.Y += hitBox.Y;
			}
			Vector2 max = min;
			if(hitbox) {
				max.X += hitBox.Width;
				max.Y += hitBox.Height;
			} else {
				max.X += width;
				max.Y += height;
			}
			return new Vector2[]{min,max};
		}
            

		public bool DetectCollision(){
            
            int[][] blocks = OverlapingBlocks();
            for(int i = 0; i < blocks.Length; i++) {
                if(blocks[i][0]>=room.width || blocks[i][1]>=room.height){
                    lastBlockDetected = null;
                    return true;
                }
                Entity block = room.blocks[blocks[i][0], blocks[i][1]];
                if(block != null && block.collision) {
                    if(TestCollision(block)) {
                        lastBlockDetected = (Block)block;
                        return true;
				    }
			    } 
            }

			return false;
		}

	}
}

