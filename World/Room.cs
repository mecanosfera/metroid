﻿using System;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Xml;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Audio;

namespace metroid {
	public class Room : BaseState{

		public World world;
		public bool loaded = false;
		public Block[,] blocks;
		public Block[] loadedBlocks;
        public List<Block> blocksUpdate;
		public Dictionary<string,Door> doors;
		public Vector2 cameraPosition;
		public int width;
		public int height;
		public int[] spawnPoint;
		public Samus player;
		public Vector2 cameraPos;
		public List<Enemy> enemies;
		public List<Enemy> unloadedEnemies;
		public int nextSpawnedEnemy = 0;
		public List<Bullet> samusBullets;
		public List<Bullet> enemyBullets;
        public List<Item> dropItens;
        public Item item;
		public Rectangle spritePosDebug;
		public float waitTimer = 0.0f;
		public int maxEnemies = 6;
		public int startY = 0;
		public int enterDirection = 1;
        public float enterDiff = 0f;
        public bool getItem = false;
        float ttime = 0;
        public bool spawning = false;


		public Room(Game1 g, World w, string n): base(g,n){
			world = w;
			game = g;
			name = n;
			spritePosDebug = spritePosDebug = new Rectangle(0, 1280, 32, 32);
			enemies = new List<Enemy>();
			unloadedEnemies = new List<Enemy>();
			samusBullets = new List<Bullet>();
			enemyBullets = new List<Bullet>();
            dropItens = new List<Item>();
            blocksUpdate = new List<Block>();
			doors = new Dictionary<string,Door>();
			//charPos = player.p;
		}

		public override void Enter(string lastState){
			player = world.player;
			if(!loaded) {
				Load(name+".tmx");
				loaded = true;
			}
			if(!world.transition) {
                if(name == "room_0"){
                    player.position = new Vector2(40 * 32, 10 * 32);
                } else if(name == "room_3"){
                    player.position = new Vector2(80, 197 * 32);
                } else if(name == "room_5"){
                    player.position = new Vector2(80, 96 * 32);
                } else {
                    player.position = new Vector2(80, 6 * 32);
                }
                Spawn(false);
                EnemySpawn();
			} else if(world.transition) {
				player.room = this;
                Door old = world.exitRoom.doors[name];
                enterDiff = (old.y*32)-player.position.Y;
                player.position.Y = (world.startTransition.Y-enterDiff);
				if(enterDirection == 1) {
					player.position.X = (doors[lastState].x * 32) + 48;
				} else {
					player.position.X = (doors[lastState].x * 32) - 36;
				}
                EnemySpawn();
			
			}
			samusBullets = player.bullets;

			//spawn enemy
		}

        public void Spawn(bool end){
            if(!end){
                spawning = true;
                game.music.Stop();
                game.music = game.musicSpawn.CreateInstance();
                game.music.IsLooped = false;
                game.music.Play();
                player.states.Change("spawn");
            } else {
                game.music.Stop();
                game.music = game.musicBrinstar.CreateInstance();
                game.music.IsLooped = true;
                game.music.Play();
                spawning = false;

            }
        }

        public void EnemySpawn(){
            foreach(Enemy e in unloadedEnemies){
                if(enemies.Count < maxEnemies){
                    enemies.Add(e);
                }
            }
            foreach(Enemy e in enemies){
                if(unloadedEnemies.Contains(e)){
                    unloadedEnemies.Remove(e);
                }
                e.Spawn(true);
            }
        }

		public virtual void Transition(Door d){
			world.Transition(this,d.nextRoom);
		}


			
		public virtual void Camera(Vector2 newPosition){
			if (!game.cameraDebug){
				Vector2 pos = newPosition;
				pos.X -= 256;
                pos.Y -= 224;
				int limitX = 0;//+(startX*32);
				if (!world.transition) {
					if (pos.X < limitX) {
						pos.X = limitX;
				    }

    				if (pos.X > ((width * 32) - limitX) - 512) {
    					pos.X = (((width * 32) - limitX) - 512);
    				}

			    }
			
                if(world.transition){
                    if(pos.Y < startY * 32){
                        pos.Y = (startY * 32) +16;
                    } else {
                        pos.Y += 80;
                    }
                } else {
                    if(pos.Y < 16){
                        pos.Y = 16;
                    } else {
                        
                        if(pos.Y > 16){
                            pos.Y += 80+enterDiff;
                        }
                    }
                }
                if(pos.Y > (((height) * 32) - 464)) {
                    pos.Y = (((height) * 32) - 464);
                } else {
                    
                }
    			if (world.transition && world.enterRoom == this) {
    				if (enterDirection == -1) {
    					if (pos.X < ((width) * 32 - limitX) - 512) {
    						pos.X = (((width) * 32 - limitX) - 512);
    						world.TransitionEnd();
    				    }
    			    } else if (enterDirection == 1) {		
        				if (pos.X > limitX) {
        					pos.X = limitX;
        					world.TransitionEnd();
        				}
    			    }
    			}
    			cameraPos = pos;

			} else {
				if (Keyboard.GetState().IsKeyDown(Keys.W)) {
					cameraPos.Y -= 10f;
				} else if (Keyboard.GetState().IsKeyDown(Keys.S)){
					cameraPos.Y += 10f;
				}

				if (Keyboard.GetState().IsKeyDown(Keys.D)) {
					cameraPos.X -= 10f;
				} else if (Keyboard.GetState().IsKeyDown(Keys.A)){
					cameraPos.X += 10f;
				}
			}
		}

        public bool OnCamera(Entity e, bool spawn = false, bool extended = false){
            Vector2[] minMaxEntity;
            Vector2 myMin;
            Vector2 myMax;
            if(!spawn){
                minMaxEntity = e.GetMinMax();
                myMin = minMaxEntity[0];
                myMax = minMaxEntity[1];
            } else {
                Enemy en = (Enemy)e;
                myMin = en.spawnPosition;
                myMax = en.spawnPosition+(new Vector2(32,32));
            }
			Vector2[] minMaxCamera = new Vector2[]{ cameraPos, cameraPos + new Vector2(512, 448) };
			
			Vector2 testMin = minMaxCamera[0];
			Vector2 testMax = minMaxCamera[1];

            if(extended){
                testMin.X -= 120;
                testMin.Y -= 120;
                testMax.X += 120;
                testMax.Y += 120;
            } else {
                testMin.X -= 40;
                testMin.Y -= 40;
                testMax.X += 40;
                testMax.Y += 40;
            }
                
            if(myMin.X >= testMin.X && myMax.X <= testMax.X &&
                myMin.Y >= testMin.Y && myMax.Y <= testMax.Y){
                return true;
            }
			return false;
		}
						

		public override void Update(GameTime gameTime){
			if(!getItem) {
				if(!world.transition) {
					player.Update(gameTime);
				}
				Camera(player.position);
				EntitiesUpdate(gameTime);
                foreach(Block b in blocksUpdate){
                    b.Update(gameTime);
                }
			} else {
                if(game.musicGetItem.State == SoundState.Stopped){
                    getItem = false;
                    game.music.Play();
                    item.taken = true;
                }
				waitTimer -= gameTime.ElapsedGameTime.Milliseconds / 10.0f;
				if(waitTimer < 0.0f) {
					waitTimer = 0.0f;
				}
			}
		}

		public virtual void EntitiesUpdate(GameTime gameTime){
            if(!spawning){
                ttime += (float)gameTime.ElapsedGameTime.TotalSeconds;
                foreach(Enemy enemy in enemies){
                    bool extendedCam = OnCamera(enemy, false, true);
                    if(!enemy.dead && extendedCam){
                        enemy.Update(gameTime);
                    } else {
                        if(enemy.dead || !extendedCam){
                            unloadedEnemies.Add(enemy);
                        }
                    }
    			}
                foreach(Enemy e in unloadedEnemies){
                    if(enemies.Contains(e)){
                        enemies.Remove(e);
                    }

                }
                foreach(Enemy e in unloadedEnemies){
                    if(enemies.Count < maxEnemies){
                        if(e.Spawn()){
                            //Console.WriteLine(ttime + "s: " + e.name);
                            enemies.Add(e);
                        }
    				}
    			}
                foreach(Enemy e in enemies){
                    if(unloadedEnemies.Contains(e)){
                        unloadedEnemies.Remove(e);
                    }
                }
    				
                foreach(Item i in dropItens){
                    if(!i.taken){
                        i.Update(gameTime);
                    }
                }
    			
                if(item != null && !item.taken){
                    item.Update(gameTime);
                }


                foreach(Bullet b in enemyBullets) {
                    if(b.active) {
                        b.Update(gameTime);
    				}
    			}
                foreach(KeyValuePair<string, Door> d in doors){
                    d.Value.Update(gameTime);
                }
            }
		}

		public override void Draw(GameTime gameTime){
			for(int x = 0; x < width; x++) {
				for(int y = 0; y < height; y++) {
					if(blocks[x,y] != null) {
						Rectangle spritePos = blocks[x, y].spritePos;
						if(game.debug) {
							spritePos = spritePosDebug;
						}
						//Console.WriteLine("bbbbb: "+cameraPos.Y);
						game.spriteBatch.Draw(
							game.tileset,
							new Vector2((x*32)-cameraPos.X,(y*32)-cameraPos.Y), //position
							spritePos, // draw rectangle
							Color.White,
							0.0f,
							Vector2.Zero,
							1.0f,
							SpriteEffects.None,
							0.5f
						);
						// depth 0.5
					}	
				}
			}
			if(!world.transition) {
                player.Draw(gameTime);	
			}

            if(!spawning && (!world.transition || (world.transition && world.enterRoom==this))){
				EntitiesDraw(gameTime);	
			}
		}

		public virtual void EntitiesDraw(GameTime gameTime){
			foreach(Enemy enemy in enemies) {
				if(!enemy.dead) {
					enemy.Draw(gameTime);
				}
			}
			foreach(Bullet b in enemyBullets) {
				if(b.active) {
					b.Draw(gameTime);
				}
			}
            foreach(Item i in dropItens){
                if(!i.taken){
                    i.Draw(gameTime);
                }
            }
            if(item != null && !item.taken){
                item.Draw(gameTime);
            }
		}


		public void Load(string filename){
			//loaded = true;
			XmlDocument doc = new XmlDocument();
			doc.Load("Content/maps/rooms/"+filename);
			XmlNodeList layers = doc.GetElementsByTagName("layer");

			//BLOCKS
			foreach(XmlNode blocksLayer in layers) {
				if(blocksLayer.Attributes["name"].Value == "blocks") {					
					width = Int32.Parse(blocksLayer.Attributes["width"].Value);
					height = Int32.Parse(blocksLayer.Attributes["height"].Value);
					blocks = new Block[width, height];
					XmlNodeList tiles = blocksLayer.FirstChild.ChildNodes;
					int x = 0;
					int y = 0;

					foreach(XmlNode t in tiles) {
						int gid = Int32.Parse(t.Attributes["gid"].Value);
						if(gid != 0) {
							blocks[x, y] = new Block(this, gid, new int[2]{ x, y });
                            if(gid == 86){
                                blocks[x, y].lava = 0;
                                blocks[x, y].hitBox = new Rectangle(0, 16, 32, 16);
                                blocks[x, y].collision = false;
                            } else if(gid == 87){
                                blocks[x, y].lava = 1;
                                blocks[x, y].collision = false;
                            } else if(gid == 114 || gid == 115){
                                blocks[x, y].hitBox = new Rectangle(0, 16, 32, 16);
                            } else if(gid == 116){
                                blocks[x, y].breakable = true;
                                blocksUpdate.Add(blocks[x, y]);
                            }
						} else {
							blocks[x, y] = null;
						}
						x++;
						if(x == width) {
							x = 0;
							y++;
						}
					}
				} 
			}

			//ENEMY
			XmlNodeList entities = doc.GetElementsByTagName("objectgroup");
			foreach(XmlNode entityGroup in entities) {
				XmlNodeList ch = entityGroup.ChildNodes;
				if(entityGroup.Attributes["name"].Value == "enemy") {
					foreach(XmlNode spawn in ch) {
						XmlNodeList spawnProperties = spawn.FirstChild.ChildNodes;
						int direction = 1;
						int rotation = 0;
						bool loaded = false;
						foreach(XmlNode property in spawnProperties) {
							if(property.Attributes["name"].Value == "direction") {
								direction = Int32.Parse(property.Attributes["value"].Value);
							} else if(property.Attributes["name"].Value == "rotation") {
								rotation = Int32.Parse(property.Attributes["value"].Value);
							} else if(property.Attributes["name"].Value == "loaded") {
								loaded = Boolean.Parse(property.Attributes["value"].Value);
							}
						}

						Enemy em = Enemy.EnemySpawner(
							this, //room
							new Vector2(//position
								Int32.Parse(spawn.Attributes["x"].Value),
								Int32.Parse(spawn.Attributes["y"].Value)
							),
							spawn.Attributes["name"].Value, //enemyName
							rotation,
							direction, //direction
							Int32.Parse(spawn.Attributes["type"].Value) //enemyType
						);

						em.loaded = loaded;
						if (em.bullets.Count > 0){
							foreach (Bullet b in em.bullets){
								enemyBullets.Add(b);
							}
						}

						if(enemies.Count < maxEnemies){
							enemies.Add(em);
						} else{
							unloadedEnemies.Add(em);
						}
                        dropItens.Add(em.drop);
					}
                
                //ITEM
				} else if(entityGroup.Attributes["name"].Value == "item") {
                    if(ch.Count > 0){
                        XmlNode it = ch[0];
                        item = new Item(
                            this,
                            it.Attributes["name"].Value,
                            it.Attributes["type"].Value,
                            new Vector2(Int32.Parse(it.Attributes["x"].Value),Int32.Parse(it.Attributes["y"].Value))
                        );
                    }

				//DOOR
				} else if(entityGroup.Attributes["name"].Value == "door") {
					foreach(XmlNode d in ch) {
						int x = Int32.Parse(d.Attributes["x"].Value) / 32;
						int y = Int32.Parse(d.Attributes["y"].Value) / 32;
						int dir = Int32.Parse(d.Attributes["type"].Value);
						string room = d.Attributes["name"].Value;
						Door door = new Door(this, room, dir);
						door.x = x;
						door.y = y;

						if(dir == -1) {
                            //Console.WriteLine(blocks[x, y].gid);
                            if(blocks[x, y].gid==645){
                                door.normal = false;
                            }
							blocks[x, y] = new DoorBlock(door, blocks[x, y].gid, new int[2]{ x, y }, false, dir);
							blocks[x, y + 1] = new DoorBlock(door, blocks[x, y + 1].gid, new int[2]{ x, y+1 }, false, dir);
							blocks[x, y + 2] = new DoorBlock(door, blocks[x, y + 2].gid, new int[2]{ x, y+2 }, false, dir);
							blocks[x + 1, y] = new DoorBlock(door, blocks[x + 1, y].gid, new int[2]{ x+1, y }, true, dir);
							blocks[x + 1, y + 1] = new DoorBlock(door, blocks[x + 1, y + 1].gid, new int[2]{ x+1, y+1 }, true, dir);
							blocks[x + 1, y + 2] = new DoorBlock(door, blocks[x + 1, y + 2].gid, new int[2]{ x+1, y+2 }, true, dir);
							door.Add((DoorBlock)blocks[x+1, y]);
							door.Add((DoorBlock)blocks[x+1, y+1]);
							door.Add((DoorBlock)blocks[x+1, y+2]);
							door.Add((DoorBlock)blocks[x, y]);
							door.Add((DoorBlock)blocks[x, y+1]);
							door.Add((DoorBlock)blocks[x, y+2]);
						} else {
                            if(blocks[x, y].gid==645){
                                door.normal = false;
                            }
							blocks[x, y] = new DoorBlock(door, blocks[x, y].gid, new int[2]{ x, y }, true, dir);
							blocks[x, y + 1] = new DoorBlock(door, blocks[x, y + 1].gid, new int[2]{ x, y+1 }, true, dir);
							blocks[x, y + 2] = new DoorBlock(door, blocks[x, y + 2].gid, new int[2]{ x, y+2 }, true, dir);
							blocks[x + 1, y] = new DoorBlock(door, blocks[x + 1, y].gid, new int[2]{ x+1, y }, false, dir);
							blocks[x + 1, y + 1] = new DoorBlock(door, blocks[x + 1, y + 1].gid, new int[2]{ x+1, y+1 }, false, dir);
							blocks[x + 1, y + 2] = new DoorBlock(door, blocks[x + 1, y + 2].gid, new int[2]{ x+1, y+2 }, false, dir);
							door.Add((DoorBlock)blocks[x+1, y]);
							door.Add((DoorBlock)blocks[x+1, y+1]);
							door.Add((DoorBlock)blocks[x+1, y+2]);
							door.Add((DoorBlock)blocks[x, y]);
							door.Add((DoorBlock)blocks[x, y+1]);
							door.Add((DoorBlock)blocks[x, y+2]);
						}
						doors.Add(room,door);
					}
				}
			}


		}

        public override void Exit(string newState){
            foreach(Enemy e in enemies){
                unloadedEnemies.Add(e);
            }
            enemies = new List<Enemy>();
            base.Exit(newState);
        }

	}
}

