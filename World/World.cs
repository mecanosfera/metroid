﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using System.IO;
using Microsoft.Xna.Framework.Graphics;

namespace metroid{
	
	public class World{

		public StateMachine rooms;
		public List<Room> roomsList;
		public Game1 game;
		public Samus player;
		public string startRoom;
		public bool transition = false;
        public bool beginTransition = false;
        public Room exitRoom;
		public Room enterRoom;
		public float transitionY;
		public Vector2 startTransition;
        public Vector2 exitTransition;
        public GameTime gTime;
        public bool startGame = false;
        public bool paused = false;


        public World(Game1 g, string sr="room_0"){
			game = g;
			roomsList = new List<Room>();
			startRoom = sr;
			rooms = new StateMachine(loadRooms());
			player = new Samus((Room)rooms.get(startRoom));
			foreach(Room r in roomsList) {
				r.player = player;
			}
			rooms.Start(startRoom);
		}

        public void Transition(Room ex, Room en){
			if(!en.loaded) {
				en.Load(en.name + ".tmx");
				en.loaded = true;
			}
			transition = true;
            beginTransition = true;
			exitRoom = ex;
			enterRoom = en;
            enterRoom.player = player;
            Door dex = exitRoom.doors[enterRoom.name];
			Door den = enterRoom.doors[exitRoom.name];
            enterRoom.startY = 0;
            enterRoom.enterDirection = enterRoom.doors[exitRoom.name].direction;
            exitTransition = player.position;
            exitTransition.Y = (dex.y * 32);
            if(exitTransition.Y > 0){
                //exitTransition.Y -= 160;
            }
            startTransition = player.position;
            startTransition.Y = (den.y * 32);
            if(startTransition.Y > 0){
                //startTransition.Y += 160;
            }
            rooms.Change(enterRoom.name);
			if(enterRoom.enterDirection == 1) {
				startTransition.X = (exitRoom.width * 32)-256;
			} else {
				startTransition.X = 256;
			}
            exitTransition.X = startTransition.X;
            //exitRoom.doors[enterRoom.name].Update(gameTime);
            enterRoom.doors[exitRoom.name].Open();

            Update(gTime);
		}

		public void TransitionEnd(){
			transition = false;
		}

        public void Pause(){
            if(paused){
                
                game.sfxGetEnergy.Play();
                game.music.Resume();
                paused = false;

            } else {
                game.sfxGetEnergy.Play();
                game.music.Pause();
                paused = true;
            }
        }


		public void Update(GameTime gameTime){
            if(!paused){
                gTime = gameTime;
                if(!transition) {
                    rooms.activeState.Update(gameTime);
			    } else {
                    int dir = enterRoom.doors[exitRoom.name].direction;
                    startTransition.X += 3.0f * dir;
                    exitTransition.X += 3.0f * dir;
                    exitRoom.Camera(exitTransition);
                    exitRoom.doors[enterRoom.name].Update(gameTime);
                    enterRoom.doors[exitRoom.name].Update(gameTime);
                    if(dir == 1) {
                        enterRoom.Camera(new Vector2((exitRoom.cameraPos.X + 256 - (exitRoom.width * 32)) * (dir), startTransition.Y));
				    } else {
                        //Console.WriteLine((enterRoom.width * 32) - exitRoom.cameraPos.X + 240);
                        enterRoom.Camera(new Vector2(((enterRoom.width * 32) + exitRoom.cameraPos.X + 256), startTransition.Y));
				    }
			    }
            } else {
                if(game.Select()){
                    game.gameState = "continue";
                    game.menuCursor.X = 30;
                    game.menuCursor.Y = 50;
                    game.startRoom = 0;
                }
            }
		}

		public void Draw(GameTime gameTime){
            game.spriteBatch.Draw(
                player.spriteSheet,
                new Vector2(36,42), //position
                new Rectangle(12,1464,56,24), // draw rectangle
                Color.White,
                0.0f,
                Vector2.Zero,
                1.0f,
                SpriteEffects.None,
                0.1f
            );
            if(player.maxEnergyTanks > 0){
                if(player.fullEnergyTanks == 0){
                    game.spriteBatch.Draw(
                        player.spriteSheet,
                        new Vector2(120,26), //position
                        new Rectangle(69,1443,16,16), // draw rectangle
                        Color.White,
                        0.0f,
                        Vector2.Zero,
                        1.0f,
                        SpriteEffects.None,
                        0.1f
                    );
                } else {
                    game.spriteBatch.Draw(
                        player.spriteSheet,
                        new Vector2(120,26), //position
                        new Rectangle(47,1443,16,16), // draw rectangle
                        Color.White,
                        0.0f,
                        Vector2.Zero,
                        1.0f,
                        SpriteEffects.None,
                        0.1f
                    );
                }
            }
            if(player.hp > 9){
                game.DrawText(("" + player.hp), new Vector2(88, 39), game.nesHud);
            } else {
                game.DrawText(("0" + player.hp), new Vector2(88, 39), game.nesHud);
            }

            if(player.maxMissiles > 0){
                if(player.missiles < 10){
                    game.DrawText(("00" + player.missiles), new Vector2(72, 58), game.nesHud);
                } else {
                    game.DrawText(("0" + player.missiles), new Vector2(72, 58), game.nesHud);
                }

                game.spriteBatch.Draw(
                    player.spriteSheet,
                    new Vector2(42,66), //position
                    new Rectangle(158,1438,22,16), // draw rectangle
                    Color.White,
                    0.0f,
                    Vector2.Zero,
                    1.0f,
                    SpriteEffects.None,
                    0.1f
                );
            }

            if(!transition) {
                rooms.activeState.Draw(gameTime);
			} else {
                if(beginTransition){
                    beginTransition = false;
                } else {
                    exitRoom.Draw(gameTime);
                }
                enterRoom.Draw(gameTime);
			}


		}
			
		public Dictionary<string,BaseState> loadRooms(){
			Dictionary<string,BaseState> rooms_dict = new Dictionary<string,BaseState>();
			string dir = "Content/maps/rooms/";
			if(Directory.Exists(dir)) {
				string[] files = Directory.GetFiles(dir);
				foreach(string f in files) {
					string filename = f.Remove(0, dir.Length);
					string roomName = filename.Remove(filename.Length-4, 4);
					Room r = new Room(game, this, roomName);
					roomsList.Add(r);
					rooms_dict.Add(roomName, r);
				}
			}
			return rooms_dict;
		}
	}
}

